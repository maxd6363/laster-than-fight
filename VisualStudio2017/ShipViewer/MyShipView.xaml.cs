﻿using Modele;
using Modele.Rooms;
using Modele.Ships;
using Sound;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace ShipViewer
{
    /// <summary>
    /// Interaction logic for MyShipView.xaml
    /// </summary>
    public partial class MyShipView : UserControl
    {
        public static DependencyProperty ShipProperty;

        private DoubleAnimation doubleAnimation;
        private Storyboard storyboard;






        static void PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is MyShipView myShipView)
            {
                myShipView.DisplayShipToGrid();
                myShipView.MainGrid.Visibility = Visibility.Visible;
                if (myShipView.Ship == null)
                {
                    myShipView.MainGrid.Visibility = Visibility.Hidden;
                }
            }
        }



        public Ship Ship
        {
            get
            {
                return GetValue(ShipProperty) as Ship;
            }
            set
            {
                SetValue(ShipProperty, value);
            }
        }

        private bool showDelete;



        public bool ShowDelete
        {
            get
            {
                return showDelete;
            }
            set
            {
                showDelete = value;
                if (showDelete == false)
                {
                    DeleteShipOnView.Visibility = Visibility.Collapsed;
                }
                else
                {
                    DeleteShipOnView.Visibility = Visibility.Visible;
                }
            }
        }



        /// <summary>
        /// static ctor to register the Dependency Property "Ship"
        /// </summary>
        static MyShipView()
        {
            ShipProperty = DependencyProperty.Register("Ship", typeof(Ship), typeof(MyShipView), new PropertyMetadata(PropertyChanged));
        }

        /// <summary>
        /// ctor of the userControl
        /// </summary>
        public MyShipView()
        {
            InitializeComponent();

            DeleteShipOnView.DataContext = this;
            CreateDynaGrid();
            InitAnimation();

            MainGrid.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// create a grid with ShipFactory.TABHEIGHT row and ShipFactory.TABWIDTH column
        /// </summary>
        public void CreateDynaGrid()
        {
            for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
            {
                ShipImageGrid.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < ShipFactory.TABWIDTH; i++)
            {
                ShipImageGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            Loaded += delegate
            {
                /*doit etre initialisé avant l'animation*/
                ShipImageGrid.Height = ShipCanvas.ActualHeight;
                ShipImageGrid.Width = ShipCanvas.ActualHeight;

                /*center grid*/
                Thickness margin = ShipImageGrid.Margin;
                margin.Left = (ShipCanvas.ActualWidth - ShipCanvas.ActualHeight) / 2;
                ShipImageGrid.Margin = margin;

            };

        }

        /// <summary>
        /// put every BitmapImage of Ship in the grid generated
        /// </summary>
        public void DisplayShipToGrid()
        {
            if (Ship == null) return;


            ShipImageGrid.Children.Clear();

            Image img;
            for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
            {
                for (int j = 0; j < ShipFactory.TABWIDTH; j++)
                {
                    if (Ship.GetRoom(i, j).Type != RoomType.NONE)
                    {
                        img = new Image
                        {
                            Source = Ship.GetRoom(i, j).ImageRoom
                        };
                        Grid.SetColumn(img, i);
                        Grid.SetRow(img, j);
                        ShipImageGrid.Children.Add(img);
                    }
                }

            }
        }



        /// <summary>
        /// init the animation of the ship
        /// </summary>
        private void InitAnimation()
        {
            doubleAnimation = new DoubleAnimation();
            storyboard = new Storyboard();

            //doubleAnimation for bouncing image
            doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            doubleAnimation.AutoReverse = true;

            //Animation depending of the current size
            doubleAnimation.From = Application.Current.MainWindow.Height / 4;
            doubleAnimation.To = Application.Current.MainWindow.Height / 3;
            doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(3));

            //storyBoard to generate animation
            storyboard.Children.Add(doubleAnimation);
            Storyboard.SetTargetName(doubleAnimation, "ShipCanvas");
            Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(Canvas.WidthProperty));

            //begin the animation
            storyboard.Begin(this);



        }

        /// <summary>
        /// Delete button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteShipOnView_Click(object sender, RoutedEventArgs e)
        {
            SoundManager.DeleteSound();
            ManagerModele.DeleteShip(Ship);
        }
    }
}
