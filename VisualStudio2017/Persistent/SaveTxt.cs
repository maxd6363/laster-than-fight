﻿using Modele.Data;
using Modele.Ressources;
using Modele.Rooms;
using Modele.Ships;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Persistent
{
    /// <summary>
    /// Save class for TXT format
    /// </summary>
    public class SaveTxt : ISave
    {
        //Ressources
        private static StreamWriter fileStreamRessource;
        private static string pathStreamRessource;

        //Ship
        private static StreamWriter fileStreamShip;
        private static string pathStreamShip;

        /// <summary>
        /// ctor, set path to save Ship and ressources
        /// </summary>
        public SaveTxt()
        {
            pathStreamRessource = Directory.GetCurrentDirectory() + @".\..\..\SavePlayer\SaveRessource.dat";
            pathStreamShip = Directory.GetCurrentDirectory() + @".\..\..\SavePlayer\SaveShip.dat";
        }




        /// <summary>
        /// save list of ressource in SaveRessource.dat
        /// </summary>
        // <param name="listRessource">list of ressources to save</param>
        public void SaveListRessource(List<Ressource> listRessource)
        {
            try
            {
                using (fileStreamRessource = new StreamWriter(pathStreamRessource))
                {
                    fileStreamRessource.WriteLine("BEGIN");
                    foreach (Ressource r in listRessource)
                    {
                        AddRessource(fileStreamRessource, r);
                    }
                    fileStreamRessource.WriteLine("END");
                }


            }
            catch (UnauthorizedAccessException) { Debug.WriteLine("can't access SaveRessource.dat (unauthorized)"); }
            catch (ArgumentException) { Debug.WriteLine("can't access SaveRessource.dat (wrong path)"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("can't access SaveRessource.dat (folder not found)"); }
            catch (PathTooLongException) { Debug.WriteLine("can't access SaveRessource.dat (path too long)"); }
            catch (IOException) { Debug.WriteLine("can't access SaveRessource.dat (Input/Output Exception)"); }
        }

        /// <summary>
        /// add one ressource to the file
        /// </summary>
        /// <param name="fileStreamRessource">StreamWriter to write</param>
        /// <param name="r">ressource to write</param>
        private void AddRessource(StreamWriter fileStreamRessource, Ressource r)
        {
            fileStreamRessource.WriteLine($"{r.Type} {r.Name} {r.NbRessourcePlayer}");
        }





        /// <summary>
        /// save list of ships in SaveShip.dat
        /// </summary>
        /// <param name="listShip">list of ship to save</param>
        public void SaveListShip(List<Ship> listShip)
        {
            try
            {
                using (fileStreamShip = new StreamWriter(pathStreamShip))
                {
                    fileStreamShip.WriteLine("BEGIN");
                    fileStreamShip.WriteLine(listShip.Count.ToString());
                    foreach (Ship s in listShip)
                    {
                        AddShip(fileStreamShip, s);
                    }
                    fileStreamShip.WriteLine("END");

                }
            }
            catch (UnauthorizedAccessException) { Debug.WriteLine("can't access SaveShip.dat (unauthorized)"); }
            catch (ArgumentException) { Debug.WriteLine("can't access SaveShip.dat (wrong path)"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (folder not found)"); }
            catch (PathTooLongException) { Debug.WriteLine("can't access SaveShip.dat (path too long)"); }
            catch (IOException) { Debug.WriteLine("can't access SaveShip.dat (Input/Output Exception)"); }

        }
        /// <summary>
        /// add one ship to the file
        /// </summary>
        /// <param name="fileStreamRessource">StreamWriter to write</param>
        /// <param name="s">ship to write</param>
        private void AddShip(StreamWriter fileStreamShip, Ship s)
        {

            if (s == null) return;
            int i = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("BEGIN_SHIP\n");
            sb.Append(s.Name + "\n");
            sb.Append(s.Description + "\n");
            sb.Append((int)s.Type + "\n");
            foreach (Room room in s.GetListOfRooms())
            {             
                sb.Append($"{((int)room.Type).ToString("X")} ");

                i = (i + 1) % ShipFactory.TABWIDTH;
                if (i == 0)
                {
                    sb.Append("\n");
                }
            }
            sb.Append("END_SHIP\n");
            fileStreamShip.Write(sb.ToString());
        }


        /// <summary>
        /// Do nothing because rooms don't need to be saved
        /// </summary>
        /// <param name="listRoom"></param>
        public void SaveListRoom(List<IRoom> listRoom)
        {
            Debug.WriteLine("Not saving room");
        }
    }
}
