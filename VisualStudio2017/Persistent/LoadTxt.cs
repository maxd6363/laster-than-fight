﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Modele.Data;
using Modele.Ressources;
using Modele.Ressources.Type.Energy;
using Modele.Ressources.Type.Electronic;
using Modele.Ressources.Type.Metal;
using Modele.Rooms;
using Modele.Ships;
using Modele.Rooms.Filler;
using Modele.Rooms.Tech;
using Modele.Rooms.Weapon;
using Modele.Rooms.Sonic;

namespace Persistent
{
    /// <summary>
    /// Load class for TXT format
    /// </summary>
    public class LoadTxt : ILoad
    {
        //Ressources
        private StreamReader fileStreamRessource;
        private readonly string pathStreamRessource;

        //Ship
        private StreamReader fileStreamShip;
        private readonly string pathStreamShip;


        /// <summary>
        /// ctor, set path to load Ship and ressources
        /// </summary>
        public LoadTxt()
        {
            pathStreamRessource = Directory.GetCurrentDirectory() + @".\..\..\SavePlayer\SaveRessource.dat";
            pathStreamShip = Directory.GetCurrentDirectory() + @".\..\..\SavePlayer\SaveShip.dat";
        }


        /// <summary>
        /// load list of ressource from file SaveRessource.dat
        /// </summary>
        /// <returns>list of ressource loaded</returns>
        public List<Ressource> LoadListRessource()
        {
            List<Ressource> listRessource = new List<Ressource>();



            foreach (RessourceMetalType rt in Enum.GetValues(typeof(RessourceMetalType)))
            {
                listRessource.Add(new RessourceMetal(rt));
            }
            foreach (RessourceEnergyType rt in Enum.GetValues(typeof(RessourceEnergyType)))
            {
                listRessource.Add(new RessourceEnergy(rt));
            }
            foreach (RessourceElectronicType rt in Enum.GetValues(typeof(RessourceElectronicType)))
            {
                listRessource.Add(new RessourceElectronic(rt));
            }



            try
            {
                using (fileStreamRessource = new StreamReader(pathStreamRessource))
                {
                    if (fileStreamRessource.ReadLine() != "BEGIN")
                    {
                        Debug.WriteLine("Error loading ressources");
                        return listRessource;
                    }

                    Ressource r = GetRessource(fileStreamRessource);

                    while (r != null)
                    {

                        listRessource[listRessource.FindIndex(res => res.Equals(r))] = r;
                        r = GetRessource(fileStreamRessource);
                    }
                }
            }
            catch (ArgumentException) { Debug.WriteLine("can't access SaveRessource.dat (wrong path)"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("can't access SaveRessource.dat (folder not found)"); }
            catch (FileNotFoundException) { Debug.WriteLine("can't access SaveRessource.dat (file not found)"); }
            catch (IOException) { Debug.WriteLine("can't access SaveRessource.dat (Input/Output Exception)"); }

            return listRessource;
        }

        /// <summary>
        /// get one ressource from the file
        /// </summary>
        /// <param name="sr">Stream reader used to read the file</param>
        /// <returns>ressource reed</returns>
        private Ressource GetRessource(StreamReader sr)
        {
            string[] words;
            string line;
            int qte = 0;
            Ressource r = null;

            line = sr.ReadLine();
            if (line != "END")
            {
                words = line.Split(' ');

                /*PARSE NUMBER OF RESSOURCE*/
                try
                {
                    qte = int.Parse(words[2]);
                }
                catch (ArgumentNullException) { Debug.WriteLine($"Null quantity for \"{line}\""); }
                catch (FormatException) { Debug.WriteLine($"Parse Error for \"{line}\""); }
                catch (OverflowException) { Debug.WriteLine($"Quantity overflow for \"{line}\""); }


                switch (words[0])
                {
                    case "METAL":
                        try
                        {
                            r = new RessourceMetal((RessourceMetalType)Enum.Parse(typeof(RessourceMetalType), words[1]));
                        }
                        catch (ArgumentNullException) { Debug.WriteLine($"Null type for \"{line}\""); }
                        catch (FormatException) { Debug.WriteLine($"Parse type for \"{line}\""); }
                        catch (OverflowException) { Debug.WriteLine($"Type overflow for \"{line}\""); }

                        break;
                    case "ENERGY":

                        try
                        {
                            r = new RessourceEnergy((RessourceEnergyType)Enum.Parse(typeof(RessourceEnergyType), words[1]));
                        }
                        catch (ArgumentNullException) { Debug.WriteLine($"Null type for \"{line}\""); }
                        catch (FormatException) { Debug.WriteLine($"Parse type for \"{line}\""); }
                        catch (OverflowException) { Debug.WriteLine($"Type overflow for \"{line}\""); }

                        break;
                    case "ELECTRONICS":

                        try
                        {
                            r = new RessourceElectronic((RessourceElectronicType)Enum.Parse(typeof(RessourceElectronicType), words[1]));
                        }
                        catch (ArgumentNullException) { Debug.WriteLine($"Null type for \"{line}\""); }
                        catch (FormatException) { Debug.WriteLine($"Parse type for \"{line}\""); }
                        catch (OverflowException) { Debug.WriteLine($"Type overflow for \"{line}\""); }

                        break;
                }
                r.AddRessource(qte);
                return r;
            }
            return null;
        }




        /// <summary>
        /// load list of ship from SaveShip.dat
        /// </summary>
        /// <returns>list of ship loaded</returns>
        public List<Ship> LoadListShip()
        {
            List<Ship> listShip = new List<Ship>();
            int nbShip = 0;
            try
            {
                using (fileStreamShip = new StreamReader(pathStreamShip))
                {
                    if (fileStreamShip.ReadLine() != "BEGIN")
                    {
                        Debug.WriteLine("Error loading ships(BEGIN is absent from the first line of SaveShip.dat)");
                        return listShip;
                    }

                    try
                    {
                        nbShip = int.Parse(fileStreamShip.ReadLine());
                    }
                    catch (ArgumentNullException) { Debug.WriteLine($"Null number of ships"); }
                    catch (FormatException) { Debug.WriteLine($"Parse Error number of ships"); }
                    catch (OverflowException) { Debug.WriteLine($"number of ships overflow"); }

                    for (int i = 0; i < nbShip; i++)
                    {
                        listShip.Add(GetShip(fileStreamShip));
                    }

                    if (fileStreamShip.ReadLine() != "END")
                    {
                        Debug.WriteLine("Warning loading ships(END is absent from the last line of SaveShip.dat)");
                        return listShip;
                    }
                }
            }
            catch (ArgumentException) { Debug.WriteLine("can't access SaveShip.dat (wrong path)"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (folder not found)"); }
            catch (FileNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (file not found)"); }
            catch (IOException) { Debug.WriteLine("can't access SaveShip.dat (Input/Output Exception)"); }

            return listShip;

        }

        /// <summary>
        /// get one ship from the file
        /// </summary>
        /// <param name="sr">StreamReader used to read the file</param>
        /// <returns>ship reed</returns>
        private Ship GetShip(StreamReader sr)
        {
            string name;
            string description;
            ShipType type;
            float weight = 0;
            float speed = 0;
            float attack = 0;
            int[,] comptabByIndex = new int[ShipFactory.TABHEIGHT, ShipFactory.TABWIDTH];



            if (sr.ReadLine() != "BEGIN_SHIP")
            {
                Debug.WriteLine("BEGIN_SHIP absent");
                return null;
            }
            name = sr.ReadLine();
            description = sr.ReadLine();

            int.TryParse(sr.ReadLine(), out int intType);
            type = (ShipType)intType;


            StringBuilder sb = new StringBuilder();
            string[] strBeforeParse;

            //read all value from file
            for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
            {
                sb.Append(sr.ReadLine());
            }
            strBeforeParse = sb.ToString().Split(' ');
            for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
            {
                for (int j = 0; j < ShipFactory.TABWIDTH; j++)
                {
                    try
                    {
                        string hexValue = strBeforeParse[i * ShipFactory.TABWIDTH + j];
                        comptabByIndex[i, j] = Convert.ToInt32(hexValue, 16);
                    }
                    catch (ArgumentNullException) { Debug.WriteLine($"Null roomtype index"); }
                    catch (FormatException) { Debug.WriteLine($"Parse Error roomtype index"); }
                    catch (OverflowException) { Debug.WriteLine($"Roomtype index overflow"); }
                    catch (IndexOutOfRangeException) { Debug.WriteLine($"Roomtype index out of bound"); }
                }
            }
            if (sr.ReadLine() != "END_SHIP")
            {
                Debug.WriteLine("END_SHIP absent");
                return null;
            }

            return ShipReconstruction.MakeAShip(name, description, type, weight, speed, attack, comptabByIndex);
        }



        /// <summary>
        /// load list of room : can't be modify
        /// </summary>
        /// <returns></returns>
        public List<IRoom> LoadListRoom()
        {
            List<IRoom> listRoom = new List<IRoom>
            {
                new RoomFiller(RoomType.FILLER_1),
                new RoomFiller(RoomType.FILLER_2),
                new RoomFiller(RoomType.FILLER_3),
                new RoomFiller(RoomType.FILLER_4),
                new RoomFiller(RoomType.FILLER_5),


                new RoomTech(RoomType.TECH_CLOAKING),
                new RoomTech(RoomType.TECH_HACKING),
                new RoomTech(RoomType.TECH_SHIELD),

                new RoomWeapon(RoomType.WEAPON_CANNON),
                new RoomWeapon(RoomType.WEAPON_FLAK),
                new RoomWeapon(RoomType.WEAPON_LASER),

                new RoomSonic(RoomType.SONIC_FUEL),
                new RoomSonic(RoomType.SONIC_ION),
                new RoomSonic(RoomType.SONIC_NUCLEAR)
            };



            return listRoom;
        }
    }
}
