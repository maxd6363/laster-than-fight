﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Media.Imaging;

namespace Modele.Ressources
{
    /// <summary>
    /// Ressource abstract class
    /// Extends : ObservableObj (two way TextBox
    /// </summary>
    public abstract class Ressource : ObservableObj, IEquatable<Ressource>, INotifyPropertyChanged, IDataErrorInfo
    {

        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public BitmapImage ImageRessource { get; protected set; }

        private int nbRessourcePlayer;
        [Required(ErrorMessage = "Number of ressource must be fill")]
        [Range(0, 1000000, ErrorMessage = "Number of ressources invalid")]
        public int NbRessourcePlayer
        {
            get { return nbRessourcePlayer; }
            set
            {
                if (nbRessourcePlayer != value)
                {
                    nbRessourcePlayer = value;
                    if (nbRessourcePlayer < 0) nbRessourcePlayer = 0;
                    NotifyPropertyChanged();
                }
            }
        }
        protected string Path { get; set; }
        public string Type { get; protected set; }



        public string this[string columnName]
        {
            get
            {

                var validationResults = new List<ValidationResult>();

                if (Validator.TryValidateProperty(
                    GetType().GetProperty(columnName).GetValue(this)
                    , new ValidationContext(this)
                    {
                        MemberName = columnName
                    }
                    , validationResults))
                    return null;

                return validationResults.First().ErrorMessage;
            }
        }

        public string Error { get; }





        /// <summary>
        /// ctor init NbRessourcePlayer to 0,path to src path
        /// </summary>
        public Ressource()
        {
            NbRessourcePlayer = 0;
            Path = Directory.GetCurrentDirectory() + @".\..\..\src\img\Ressources\";

        }



        /// <summary>
        /// Loads Bitmap Image base on Type
        /// </summary>
        public void LoadBitmapImage()
        {
            try
            {
                ImageRessource = new BitmapImage(new Uri(Path));
            }
            catch (DirectoryNotFoundException) { Debug.WriteLine($"Can't find {Path}(folder)"); }
            catch (WebException)
            {
                Debug.WriteLine($"Can't find {Path}(web)");
                ImageRessource = new BitmapImage();
            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine($"Can't find {Path}(file)");
                try
                {
                    ImageRessource = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\NAN.png"));
                }
                catch (DirectoryNotFoundException) { Debug.WriteLine($"Can't find {Path}(folder)"); }
                catch (FileNotFoundException)
                {
                    Debug.WriteLine($"Can't find {Path}(file)");
                    ImageRessource = null;
                }

            }
        }

        /// <summary>
        /// Add resource to NbRessourcePlayer
        /// </summary>
        /// <param name="qte">Quantity of ressource to add or substracte if qte lt 0 (but NbRessourcePlayer can't be negative)</param>
        public void AddRessource(int qte)
        {
            NbRessourcePlayer += qte;
            if (NbRessourcePlayer < 0)
            {
                NbRessourcePlayer = 0;
            }
        }



        public override string ToString()
        {
            return $"Ressource : {Name} - {NbRessourcePlayer}";
        }



        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.GetType() == GetType()) return Equals(obj as Ressource);
            return false;
        }


        /// <summary>
        /// Equals based on Type
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Ressource other)
        {
            if (other == null) return false;
            if (Name == other.Name && Type == other.Type) return true;
            return false;
        }

        /// <summary>
        /// HashCode based on Type
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Type.GetHashCode() * 4536;
        }
    }
}
