﻿namespace Modele.Ressources.Type.Electronic
{
    /// <summary>
    /// Enum of RessourceElectronic Types
    /// </summary>
    public enum RessourceElectronicType
    {
       COPPER_WIRE,
       PRINTED_CIRCUITS,
       ROTOR
    }
}
