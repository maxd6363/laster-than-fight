﻿namespace Modele.Ressources.Type.Electronic
{
    /// <summary>
    /// Ressource electronic, Extend Ressource
    /// Type is the type of resource (ELECTRONIC)
    /// "RealType" is the exact resource (ex : COPPER_WIRE)
    /// </summary>
    public class RessourceElectronic : Ressource
    {

        public RessourceElectronicType RealType { get; private set; }

        /// <summary>
        /// RessourceElectronic'sConstuctor 
        /// </summary>
        /// <param name="rt">type of RessourceElectronic</param>
        public RessourceElectronic(RessourceElectronicType rt) : base()
        {
            RealType = rt;
            Type = "ELECTRONICS";
            Name += $"{rt.ToString()}";
            Path += $@"electronics\{RealType.ToString().ToUpper()}.png";
            SetDescription();
            base.LoadBitmapImage();
        }

        /// <summary>
        /// Set the description of the ressource based on RealType
        /// </summary>
        private void SetDescription()
        {
            switch (RealType)
            {
                case RessourceElectronicType.COPPER_WIRE:
                    Description = "Usefull, light and cheap copper wires";
                    break;
                case RessourceElectronicType.PRINTED_CIRCUITS:
                    Description = "Pretty sure it's not supposed to beep like that ?";
                    break;
                case RessourceElectronicType.ROTOR:
                    Description = "It seems to be rotating. Astonishing.";
                    break;
                default: Description = "What is this ?";
                    break;


            }
        }
    }
}
