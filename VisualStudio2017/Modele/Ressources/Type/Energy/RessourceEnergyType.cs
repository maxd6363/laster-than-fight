﻿namespace Modele.Ressources.Type.Energy
{
    /// <summary>
    /// Enum of RessourceEnergyType Types
    /// </summary>
    public enum RessourceEnergyType
    {
        ION_GENERATOR,
        FUEL,
        NUCLEAR_ROD
       
    }
}
