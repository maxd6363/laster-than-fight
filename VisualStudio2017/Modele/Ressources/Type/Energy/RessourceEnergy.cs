﻿namespace Modele.Ressources.Type.Energy
{
    /// <summary>
    /// Ressource Energy, Extend Ressource
    /// Type is the type of resource (ENERGY)
    /// "RealType" is the exact resource (ex : FUEL)
    /// </summary>
    public class RessourceEnergy : Ressource
    {
        public RessourceEnergyType RealType { get; private set; }

        /// <summary>
        /// RessourceElectronic construcor'
        /// </summary>
        /// <param name="rt">type of RessourceEnergy</param>
        public RessourceEnergy(RessourceEnergyType rt) : base()
        {
            RealType = rt;
            Type = "ENERGY";
            Name += $"{rt.ToString()}";
            Path += $@"energy\{RealType.ToString().ToUpper()}.png";
            SetDescription();
            base.LoadBitmapImage();
        }
        /// <summary>
        /// Set the description of the ressource based on RealType
        /// </summary>
        private void SetDescription()
        {
            switch (RealType)
            {
                case RessourceEnergyType.ION_GENERATOR:
                    Description = "Warning \n High stun or memory loss probability if you look at it for too long";
                    break;
                case RessourceEnergyType.FUEL:
                    Description = "Black gold. \n More expensive than ever before.";
                    break;
                case RessourceEnergyType.NUCLEAR_ROD:
                    Description = "A nuclear reactor,\n formerly known as an atomic pile";
                    break;
                default:
                    Description = "What is this ?";
                    break;
            }
        }
    }
}
