﻿namespace Modele.Ressources.Type.Metal
{
    /// <summary>
    /// Enum of RessourceMetalType Types
    /// </summary>
    public enum RessourceMetalType
    {
        IRON,
        SCRAPS,
        MIRRIDIUM

    }
}
