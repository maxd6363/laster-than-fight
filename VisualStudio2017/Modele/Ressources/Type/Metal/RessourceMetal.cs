﻿namespace Modele.Ressources.Type.Metal
{
    /// <summary>
    /// Ressource Metal, Extend Ressource
    /// Type is the type of resource (METAL)
    /// "RealType" is the exact resource (ex : IRON)
    /// </summary>
    public class RessourceMetal : Ressource
    {

        public RessourceMetalType RealType { get; private set; }

        /// <summary>
        /// RessourceMetal constructor
        /// </summary>
        /// <param name="rt">type of RessourceMetal</param>
        public RessourceMetal(RessourceMetalType rt) : base()
        {
            RealType = rt;
            Type = "METAL";
            Name += $"{rt.ToString()}";
            Path += $@"metal\{RealType.ToString().ToUpper()}.png";
            SetDescription();
            base.LoadBitmapImage();
        }

        /// <summary>
        /// Set the description of the ressource based on RealType
        /// </summary>
        private void SetDescription()
        {
            switch (RealType)
            {

                case RessourceMetalType.IRON:
                    Description = "Iron plates : easy to produce, always usefull";
                    break;         
                case RessourceMetalType.SCRAPS:
                    Description = "I'm sure we'll find a way to recycle that.";
                    break;      
                case RessourceMetalType.MIRRIDIUM:
                    Description = "A strange, slightly glowing metal. I wouldn't lick it if I were you.";
                    break;
                default: Description = "What is this ?";
                    break;


            }
        }



    }
}
