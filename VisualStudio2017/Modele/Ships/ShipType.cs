﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modele.Ships
{
    /// <summary>
    /// enum for Ship Type
    /// </summary>
    public enum ShipType
    {
        SONIC,
        TECH,
        WEAPON,
        ALL
    }
}
