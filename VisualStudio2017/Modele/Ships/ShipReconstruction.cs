﻿using Modele.Rooms;
using Modele.Rooms.Filler;
using Modele.Rooms.Sonic;
using Modele.Rooms.Tech;
using Modele.Rooms.Weapon;

namespace Modele.Ships
{
    public class ShipReconstruction : IShipFactory
    {

        public static int TABHEIGHT = ShipFactory.TABHEIGHT;
        public static int TABWIDTH = ShipFactory.TABWIDTH;

        /// <summary>
        /// restaure ship from file
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="description">descrition</param>
        /// <param name="weight">weight statistic</param>
        /// <param name="speed">speed statistic</param>
        /// <param name="attack">attack statistic</param>
        /// <param name="comptabByIndex">tab of Id of rooms inside ship</param>
        /// <returns></returns>
        public static Ship MakeAShip(string name, string description,ShipType type, float weight, float speed, float attack, int[,] comptabByIndex)
        {
            Ship ship = new Ship(name, description,type, weight, speed, attack);

            RegenerateShip(ship, comptabByIndex);
            CalcCharacteristic(ship);
            return ship;
        }

        /// <summary>
        /// calculate Statistic of ships (Speed, Weight, Attack) + real number of room (security)
        /// </summary>
        /// <param name="ship">ship to use for calculation</param>
        public static void CalcCharacteristic(Ship ship)
        {
            ship.SetNumberOfRoom(0);
            for (int i = 0; i < TABHEIGHT; i++)
            {
                for (int j = 0; j < TABWIDTH; j++)
                {
                    if (ship.CompositionTab[i, j].Type != RoomType.NONE)
                    {
                        ship.Stat.Weight += ship.CompositionTab[i, j].Stat.Weight;
                        ship.Stat.Speed += ship.CompositionTab[i, j].Stat.Speed;
                        ship.Stat.Attack += ship.CompositionTab[i, j].Stat.Attack;
                        ship.SetNumberOfRoom(ship.NumberOfRoom + 1);
                    }
                }
            }
        }
        
        /// <summary>
        /// fill the ship with room from array of Id of room
        /// </summary>
        /// <param name="ship">ship to regenerate</param>
        /// <param name="comptabByIndex">array of ID of room</param>
        public static void RegenerateShip(Ship ship, int[,] comptabByIndex)
        {

            ship.CompositionTab = new Room[ShipFactory.TABHEIGHT, ShipFactory.TABWIDTH];
            for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
            {
                for (int j = 0; j < ShipFactory.TABWIDTH; j++)
                {
                    if (comptabByIndex[i, j] != 0) ship.SetNumberOfRoom(ship.NumberOfRoom + 1);


                    //little tricky but ...meh...
                    if (((RoomType)comptabByIndex[i, j]).ToString().Contains("SONIC"))
                    {
                        ship.CompositionTab[i, j] = new RoomSonic((RoomType)comptabByIndex[i, j]);
                    }
                    else if (((RoomType)comptabByIndex[i, j]).ToString().Contains("TECH"))
                    {
                        ship.CompositionTab[i, j] = new RoomTech((RoomType)comptabByIndex[i, j]);
                    }
                    else if (((RoomType)comptabByIndex[i, j]).ToString().Contains("WEAPON"))
                    {
                        ship.CompositionTab[i, j] = new RoomWeapon((RoomType)comptabByIndex[i, j]);
                    }
                    else if (((RoomType)comptabByIndex[i, j]).ToString().Contains("FILLER"))
                    {
                        ship.CompositionTab[i, j] = new RoomFiller((RoomType)comptabByIndex[i, j]);
                    }
                    else
                    {
                        //NONE ROOM
                        ship.CompositionTab[i, j] = new Room();
                    }

                }
            }


            
        }
        

    }
}
