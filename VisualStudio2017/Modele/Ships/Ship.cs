﻿using Modele.Rooms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;

namespace Modele.Ships
{
    /// <summary>
    /// ship class
    /// </summary>
    public class Ship : IShip, INotifyPropertyChanged, IEquatable<Ship>
    {

        public Stat Stat { get; private set; }

        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                if (description != value)
                {
                    if (value == "")
                    {
                        value = ShipFactory.DefaultDescription(this);
                    }
                    description = Regex.Replace(value, @"\n", "\\n");
                    NotifyPropertyChanged();
                }
            }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    if (value == "")
                    {
                        value = "Ship";
                    }
                    name = Regex.Replace(value, @"\t|\n|\r", "");
                    NotifyPropertyChanged();
                }

            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public int NumberOfRoom { get; internal set; }
        public Room[,] CompositionTab { get; internal set; }
        public ShipType Type { get; }
        public BitmapImage ImageType { get; private set; }



        /// <summary>
        /// ctor for ship
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="nbRoom">number of room</param>
        /// <param name="stat">statistic</param>
        /// <param name="type">type</param>
        /// <param name="compositionTab">coposition of ship (Rooms)</param>
        public Ship(string name, int nbRoom, Stat stat, ShipType type, Room[,] compositionTab)
        {
            Name = name;
            NumberOfRoom = nbRoom;
            Stat = stat;
            Type = type;
            CompositionTab = compositionTab;

            ShipTypeImage.DicoShipTypeBmp.TryGetValue(Type.ToString().ToUpper(), out BitmapImage btm);
            ImageType = btm;


        }



        /// <summary>
        /// ctor used for loading in ShipReconstruction
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="description">description</param>
        /// <param name="weight">weight statistic</param>
        /// <param name="speed">speed statistic</param>
        /// <param name="attack">attack statistic</param>
        public Ship(string name, string description, ShipType type, float weight, float speed, float attack)
        {
            this.Stat = new Stat();
            this.NumberOfRoom = 0;

            if (name == null || name == "") name = "Ship";
            this.Name = name;

            if (description == null || description == "") description = "Description not set";
            Description = description;

            this.Stat.Weight = weight;
            this.Stat.Speed = speed;
            this.Stat.Attack = attack;

            Type = type;
            ShipTypeImage.DicoShipTypeBmp.TryGetValue(Type.ToString().ToUpper(), out BitmapImage btm);
            ImageType = btm;
        }

        /// <summary>
        /// send event when property changed
        /// </summary>
        /// <param name="propertyName">name of property</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        
        /// <summary>
        /// get current room of ship at X-Y position in compositionTab
        /// </summary>
        /// <param name="x">X posititon</param>
        /// <param name="y">Y posititon</param>
        /// <returns>room of ship at X-Y position in compositionTab</returns>
        public Room GetRoom(int x, int y)
        {
            return CompositionTab[y, x];
        }

        /// <summary>
        /// compositionTable return as List<Room>
        /// </summary>
        /// <returns>compositionTable as List<Room></returns>
        public List<Room> GetListOfRooms()
        {
            List<Room> list = new List<Room>();
            for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
            {
                for (int j = 0; j < ShipFactory.TABWIDTH; j++)
                {
                    //set ship in right orientation (j,i)
                    list.Add(GetRoom(j, i));
                }
            }
            return list;
        }

        /// <summary>
        /// set the number of room of the ship, if negative -> set to -1
        /// </summary>
        /// <param name="numberOfRoom">number of room</param>
        internal void SetNumberOfRoom(int numberOfRoom)
        {
            if (numberOfRoom < 0)
            {
                Debug.WriteLine("Number of room invalid");
                numberOfRoom = -1;
            }
            NumberOfRoom = numberOfRoom;
        }

        public override string ToString()
        {
            return $"Ship : {Name}\nRoom : {NumberOfRoom}\n({Stat.Weight},{Stat.Speed},{Stat.Attack})";
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.GetType() == GetType()) return Equals(obj as Ship);
            return false;
        }


        public bool Equals(Ship other)
        {
            if (other == null) { return false; }
            if (Name == other.Name
                && NumberOfRoom == other.NumberOfRoom
                && Description == other.Description
                && Stat.Weight == other.Stat.Weight
                && Stat.Speed == other.Stat.Speed
                && Stat.Attack == other.Stat.Attack)
            {
                return true;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode()
                * NumberOfRoom.GetHashCode()
                * Description.GetHashCode()
                * Stat.Weight.GetHashCode()
                * Stat.Speed.GetHashCode()
                * Stat.Attack.GetHashCode();
        }

    }

}
