﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Modele.Ships
{
    public static class ShipTypeImage
    {
        public static Dictionary<string, BitmapImage> DicoShipTypeBmp { get; private set; }
        internal static string Path { get; set; }

        static ShipTypeImage()
        {
            DicoShipTypeBmp = new Dictionary<string, BitmapImage>();
            Path = Directory.GetCurrentDirectory() + @".\..\..\src\img\ShipType\";

            LoadBitmapImage();

        }


        private static void LoadBitmapImage()
        {
            foreach (ShipType st in Enum.GetValues(typeof(ShipType)))
            {
                try
                {

                    DicoShipTypeBmp.Add(st.ToString().ToUpper(), new BitmapImage(new Uri(Path + st.ToString().ToUpper() + ".png")));

                }
                //web excepetion occured one time
                catch (Exception e) when (e is DirectoryNotFoundException || e is FileNotFoundException || e is WebException)
                {
                    //Image not found
                    try
                    {
                        DicoShipTypeBmp.Add(st.ToString().ToUpper(), new BitmapImage(new Uri(Path + "../NAN.png")));
                    }
                    catch (DirectoryNotFoundException)
                    {
                        Debug.WriteLine("Can't load NAN.png (folder)");
                        DicoShipTypeBmp.Add(st.ToString().ToUpper(), null);
                    }
                    catch (FileNotFoundException)
                    {
                        Debug.WriteLine("Can't load NAN.png (file)");
                        DicoShipTypeBmp.Add(st.ToString().ToUpper(), null);
                    }
                }
            }
        }


    }
}
