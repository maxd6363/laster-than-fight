﻿using Modele.Rooms;
using System.Collections.Generic;

namespace Modele.Ships
{
    /// <summary>
    /// interface of ships
    /// </summary>
    public interface IShip
    {
        List<Room> GetListOfRooms();
        Room GetRoom(int x, int y);

     }
}

