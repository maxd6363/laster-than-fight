﻿using Modele.Rooms;
using Modele.Rooms.Filler;
using Modele.Rooms.Sonic;
using Modele.Rooms.Tech;
using Modele.Rooms.Weapon;
using System;
using System.Diagnostics;
using System.Linq;
using Modele.Ressources;
using System.Collections.Generic;
using System.IO;

namespace Modele.Ships
{
    /// <summary>
    /// ship factory class, Implement IShipFactory
    /// </summary>
    public class ShipFactory : IShipFactory
    {

        public static int TABHEIGHT = 20;
        public static int TABWIDTH = 20;
        private static readonly Random random = new Random();

        /// <summary>
        /// generate a ship
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="nbRoom">number of rooms</param>
        /// <param name="type">Type (ShipType)</param>
        /// <param name="percentFiller">percentage of filler room (if possible with ressources available) </param>
        /// <returns>Ship generated</returns>
        public static Ship MakeAShip(string name, int nbRoom, ShipType type, int percentFiller)
        {
            Stat stat = new Stat();

            //Default name
            if (name == null) { name = RandomNamePick(); }

            //Default nbRoom
            if (nbRoom < 0) { nbRoom = 1; }
            Room[,] compositionTab = new Room[TABHEIGHT, TABWIDTH];
            for (int i = 0; i < TABHEIGHT; i++)
            {
                for (int j = 0; j < TABWIDTH; j++)
                {
                    compositionTab[i, j] = new Room();
                }
            }
            Ship myNewShip = new Ship(name, nbRoom, stat, type, compositionTab);
            GenerateShip(myNewShip, percentFiller);
            CalcCharacteristic(myNewShip);

            //Default description
            myNewShip.Description = DefaultDescription(myNewShip);
            return myNewShip;
        }

        /// <summary>
        /// pick a random name of ShipName.dat
        /// </summary>
        /// <returns>Name picked</returns>
        private static string RandomNamePick()
        {
            int nbName = 1;
            try
            {
                using (StreamReader fileStreamName = new StreamReader(Directory.GetCurrentDirectory() + @".\..\..\src\text\ShipName.dat"))
                {
                    try
                    {
                        nbName = int.Parse(fileStreamName.ReadLine());
                    }
                    catch (ArgumentNullException) { Debug.WriteLine($"Null number of names"); }
                    catch (FormatException) { Debug.WriteLine($"Parse Error number of names"); }
                    catch (OverflowException) { Debug.WriteLine($"number of names overflow"); }

                    int x = random.Next(1, nbName);
                    for (int i = 0; i < x; i++)
                    {
                        fileStreamName.ReadLine();
                    }
                    return fileStreamName.ReadLine();
                }
            }
            catch (ArgumentException) { Debug.WriteLine("can't access SaveShip.dat (wrong path)"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (folder not found)"); }
            catch (FileNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (file not found)"); }
            catch (IOException) { Debug.WriteLine("can't access SaveShip.dat (Input/Output Exception)"); }
            return "Ship";
        }

        /// <summary>
        /// calculate Statistic of ships (Speed, Weight, Attack) + real number of room (security)
        /// </summary>
        /// <param name="ship"></param>
        public static void CalcCharacteristic(Ship ship)
        {
            ship.SetNumberOfRoom(0);
            for (int i = 0; i < TABHEIGHT; i++)
            {
                for (int j = 0; j < TABWIDTH; j++)
                {
                    if (ship.CompositionTab[i, j].Type != RoomType.NONE)
                    {
                        ship.Stat.Weight += ship.CompositionTab[i, j].Stat.Weight;
                        ship.Stat.Speed += ship.CompositionTab[i, j].Stat.Speed;
                        ship.Stat.Attack += ship.CompositionTab[i, j].Stat.Attack;
                        ship.SetNumberOfRoom(ship.NumberOfRoom + 1);
                    }
                }
            }
        }




        /// <summary>
        /// main methode to generate Ship composition
        /// </summary>
        /// <param name="ship">ship to generate</param>
        /// <param name="percentFiller">percentage of filler rooms</param>
        private static void GenerateShip(Ship ship, int percentFiller)
        {
            Coord c = new Coord(TABHEIGHT / 2, TABWIDTH / 2);
            int realNbRoom = 0;
            int antiInfinit = 0;

            while (realNbRoom < ship.NumberOfRoom)
            {
                if (antiInfinit == TABHEIGHT * TABWIDTH * 5) return;
                //allow almost every ship to generate with the right number of room while avoiding infinit loop
                antiInfinit++;
                RoomType roomType = GenRandomRoomType(ship.Type, percentFiller);
                if (SetRoomByCoord(c, roomType, ship))
                {
                    realNbRoom++;
                    DeleteRessource(roomType);
                }
                c = AddCoord(c, RandCoordTurtle());
            }
        }
        /// <summary>
        /// set room to coordonate in the ship
        /// </summary>
        /// <param name="c">coordonate</param>
        /// <param name="type">type of room</param>
        /// <param name="ship">ship to set the room</param>
        /// <returns>false : if failed, true : if success</returns>
        private static bool SetRoomByCoord(Coord c, RoomType type, Ship ship)
        {
            if (type == RoomType.NONE)
            {
                return false;
            }
            try
            {
                if (ship.CompositionTab[c.CoordY, c.CoordX].Type != RoomType.NONE)
                {
                    return false;
                }


                if (type.ToString().Contains("SONIC"))
                {
                    ship.CompositionTab[c.CoordY, c.CoordX] = new RoomSonic(type);
                }
                else if (type.ToString().Contains("TECH"))
                {
                    ship.CompositionTab[c.CoordY, c.CoordX] = new RoomTech(type);
                }
                else if (type.ToString().Contains("WEAPON"))
                {
                    ship.CompositionTab[c.CoordY, c.CoordX] = new RoomWeapon(type);
                }
                else if (type.ToString().Contains("FILLER"))
                {
                    ship.CompositionTab[c.CoordY, c.CoordX] = new RoomFiller(type);
                }


                return true;
            }
            catch (Exception) { Debug.WriteLine(c); }
            return false;
        }

        /// <summary>
        /// main methode to set room to new coordonate (it's a walker inside compositionTab)
        /// </summary>
        /// <returns>coordonate generated</returns>
        private static Coord RandCoordTurtle()
        {
            int X;
            int Y;

            X = (int)(random.NextDouble() * 3 - 1.5);
            if (X == 0)
            {
                Y = (int)(random.NextDouble() * 3 - 1.5);
            }
            else
            {
                Y = 0;
            }

            Coord c = new Coord(X, Y);

            return c;

        }
        /// <summary>
        /// add 2 coordonates, handle out of bounds
        /// </summary>
        /// <param name="a">Coordonate A</param>
        /// <param name="b">Coordonate B</param>
        /// <returns>addition of Coordonate A and B</returns>
        private static Coord AddCoord(Coord a, Coord b)
        {
            int X = a.CoordX + b.CoordX;
            int Y = a.CoordY + b.CoordY;

            if (X < 0) X = -X;
            if (Y < 0) Y = -Y;

            if (X >= TABWIDTH - 1) X -= TABWIDTH - X;
            if (Y >= TABHEIGHT - 1) Y -= TABHEIGHT - Y;

            return new Coord(X, Y);
        }

        /// <summary>
        /// get random room type  for ship of ShipType
        /// </summary>
        /// <param name="type">Type of ship</param>
        /// <param name="percentFiller">percentage of filler rooms</param>
        /// <returns></returns>
        private static RoomType GenRandomRoomType(ShipType type, int percentFiller)
        {
            //Génération d'un entier random compris entre 0 & 100
            int x = random.Next(0, 100);

            // Probabilité de mettre une salle "filler"
            if (x < percentFiller)
            {
                //Génération d'un entier random compris entre 1 et 5 inclus.
                x = random.Next(1, 6);
                switch (x)
                {
                    case 1: return RoomType.FILLER_1;
                    case 2: return RoomType.FILLER_2;
                    case 3: return RoomType.FILLER_3;
                    case 4: return RoomType.FILLER_4;
                    case 5: return RoomType.FILLER_5;
                }
            }

            else
            {
                //Génération d'un entier random compris entre 0 et 2 inclus.
                x = random.Next(0, 3);
                if (type == ShipType.ALL)
                {
                    return GenRandomRoomTypeAll();
                }

                while (x != -1)
                {
                    if (type == ShipType.SONIC)
                    {
                        switch (x)
                        {
                            case 0: // Cost : Scraps + CopperWire + Fuel = SONIC_FUEL                         
                                if (HandleRoomCost(new RoomSonic(RoomType.SONIC_FUEL))) { return RoomType.SONIC_FUEL; }
                                else { x = 1; }
                                //marche en cascade : si ne peux pas faire case 1 -> va à case 1 ou 2
                                break;

                            case 1: // Cost : Mirridium + Rotor + IonGen = SONIC_ION
                                if (HandleRoomCost(new RoomSonic(RoomType.SONIC_ION))) { return RoomType.SONIC_ION; }
                                else { x = 2; }
                                //marche en cascade : si ne peux pas faire case 2 -> va à case 0 ou 2 (0 * 2 ou 1 * 2)
                                break;

                            case 2: // Cost : IronPlates + PrintedCircuits + NuclearRod = SONIC_NUCLEAR
                                if (HandleRoomCost(new RoomSonic(RoomType.SONIC_NUCLEAR))) { return RoomType.SONIC_NUCLEAR; }
                                else { x = -1; }
                                //Si impossible : sort du switch
                                break;
                        }
                    }

                    if (type == ShipType.TECH)
                    {
                        switch (x)
                        {
                            case 0: // Cost : Mirridium + CopperWire + NuclearRod = TECH_CLOAKING                         
                                if (HandleRoomCost(new RoomTech(RoomType.TECH_CLOAKING))) { return RoomType.TECH_CLOAKING; }
                                else { x = 1; }
                                //marche en cascade : si ne peux pas faire case 1 -> va à case 1 ou 2
                                break;

                            case 1: // Cost : IronPlates + PrintedCir + IonGen = TECH_HACKING
                                if (HandleRoomCost(new RoomTech(RoomType.TECH_HACKING))) { return RoomType.TECH_HACKING; }
                                else { x = 2; }
                                //marche en cascade : si ne peux pas faire case 2 -> va à case 0 ou 2 (0 * 2 ou 1 * 2)
                                break;

                            case 2: // Cost : Scraps + Rotor + Fuel = TECH_SHIELD
                                if (HandleRoomCost(new RoomTech(RoomType.TECH_SHIELD))) { return RoomType.TECH_SHIELD; }
                                else { x = -1; }
                                //Si impossible : sort du switch
                                break;
                        }
                    }

                    if (type == ShipType.WEAPON)
                    {
                        switch (x)
                        {
                            case 0: // Cost : IronPlates + CopperWire + Fuel = WEAPON_CANNON                         
                                if (HandleRoomCost(new RoomWeapon(RoomType.WEAPON_CANNON))) { return RoomType.WEAPON_CANNON; }
                                else { x = 1; }
                                //marche en cascade : si ne peux pas faire case 1 -> va à case 1 ou 2
                                break;

                            case 1: // Cost : Scraps + Rotor + NuclearRod = WEAPON_FLAK
                                if (HandleRoomCost(new RoomWeapon(RoomType.WEAPON_FLAK))) { return RoomType.WEAPON_FLAK; }
                                else { x = 2; }
                                //marche en cascade : si ne peux pas faire case 2 -> va à case 0 ou 2 (0 * 2 ou 1 * 2)
                                break;

                            case 2: // Cost : Mirridium + PrintedCir + IonGen = WEAPON_LASER
                                if (HandleRoomCost(new RoomWeapon(RoomType.WEAPON_LASER))) { return RoomType.WEAPON_LASER; }
                                else { x = -1; }
                                //Si impossible : sort du switch
                                break;
                        }
                    }
                }
            }

            //fill the ship with filler when no ressources available
            x = random.Next(1, 6);
            switch (x)
            {
                case 1: return RoomType.FILLER_1;
                case 2: return RoomType.FILLER_2;
                case 3: return RoomType.FILLER_3;
                case 4: return RoomType.FILLER_4;
                default: return RoomType.FILLER_5;
            }
        }

        /// <summary>
        /// check if enough ressources to generate this room
        /// </summary>
        /// <param name="room">room to check</param>
        /// <returns>true : if enough ressources, false : if not </returns>
        private static bool HandleRoomCost(Room room)
        {
            foreach (KeyValuePair<Ressource, int> kvp in room.RoomCostDic)
            {
                if (ManagerModele.ListRessource.Last(res => res.Equals(kvp.Key)).NbRessourcePlayer < kvp.Value)
                {
                    //not enough ressource for one of the room
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// delete ressources used by room of Type roomType
        /// </summary>
        /// <param name="roomType">type of room</param>
        private static void DeleteRessource(RoomType roomType)
        {
            Room room;

            if (roomType.ToString().Contains("SONIC"))
            {
                room = new RoomSonic(roomType);
            }
            else if (roomType.ToString().Contains("TECH"))
            {
                room = new RoomTech(roomType);
            }
            else if (roomType.ToString().Contains("WEAPON"))
            {
                room = new RoomWeapon(roomType);
            }
            else
            {
                room = new RoomFiller(roomType);
            }


            foreach (KeyValuePair<Ressource, int> kvp in room.RoomCostDic)
            {
                ManagerModele.ListRessource.Last(res => res.Equals(kvp.Key)).AddRessource(-kvp.Value);
            }
        }

        /// <summary>
        /// get random room type for ship of ALL ShipType
        /// </summary>
        /// <returns>RoomType generated</returns>
        private static RoomType GenRandomRoomTypeAll()
        {
            int x = random.Next(0, 9);
            while (x != -1)
            {
                switch (x)
                {
                    case 0: // Cost : Scraps + CopperWire + Fuel = SONIC_FUEL                         
                        if (HandleRoomCost(new RoomSonic(RoomType.SONIC_FUEL))) { return RoomType.SONIC_FUEL; }
                        else { x = 1; }
                        //marche en cascade : si ne peux pas faire case 1 -> va à case 1 ou 2
                        break;

                    case 1: // Cost : Mirridium + Rotor + IonGen = SONIC_ION
                        if (HandleRoomCost(new RoomSonic(RoomType.SONIC_ION))) { return RoomType.SONIC_ION; }
                        else { x = 2; }
                        //marche en cascade : si ne peux pas faire case 2 -> va à case 0 ou 2 (0 * 2 ou 1 * 2)
                        break;

                    case 2: // Cost : IronPlates + PrintedCircuits + NuclearRod = SONIC_NUCLEAR
                        if (HandleRoomCost(new RoomSonic(RoomType.SONIC_NUCLEAR))) { return RoomType.SONIC_NUCLEAR; }
                        else { x = 3; }
                        //marche en cascade : si ne peux pas faire case 2 -> va à la case 1 ou 2
                        break;


                    case 3: // Cost : Mirridium + CopperWire + NuclearRod = TECH_CLOAKING                         
                        if (HandleRoomCost(new RoomTech(RoomType.TECH_CLOAKING))) { return RoomType.TECH_CLOAKING; }
                        else { x = 4; }
                        //marche en cascade : si ne peux pas faire case 1 -> va à case 1 ou 2
                        break;

                    case 4: // Cost : IronPlates + PrintedCir + IonGen = TECH_HACKING
                        if (HandleRoomCost(new RoomTech(RoomType.TECH_HACKING))) { return RoomType.TECH_HACKING; }
                        else { x = 5; }
                        //marche en cascade : si ne peux pas faire case 2 -> va à case 0 ou 2 (0 * 2 ou 1 * 2)
                        break;

                    case 5: // Cost : Scraps + Rotor + Fuel = TECH_SHIELD
                        if (HandleRoomCost(new RoomTech(RoomType.TECH_SHIELD))) { return RoomType.TECH_SHIELD; }
                        else { x = 6; }
                        //marche en cascade : si ne peux pas faire case 2 -> va à la case 1 ou 2
                        break;

                    case 6: // Cost : IronPlates + CopperWire + Fuel = WEAPON_CANNON                         
                        if (HandleRoomCost(new RoomWeapon(RoomType.WEAPON_CANNON))) { return RoomType.WEAPON_CANNON; }
                        else { x = 7; }
                        //marche en cascade : si ne peux pas faire case 1 -> va à case 1 ou 2
                        break;

                    case 7: // Cost : Scraps + Rotor + NuclearRod = WEAPON_FLAK
                        if (HandleRoomCost(new RoomWeapon(RoomType.WEAPON_FLAK))) { return RoomType.WEAPON_FLAK; }
                        else { x = 8; }
                        //marche en cascade : si ne peux pas faire case 2 -> va à case 0 ou 2 (0 * 2 ou 1 * 2)
                        break;

                    case 8: // Cost : Mirridium + PrintedCir + IonGen = WEAPON_LASER
                        if (HandleRoomCost(new RoomWeapon(RoomType.WEAPON_LASER))) { return RoomType.WEAPON_LASER; }
                        else { x = -1; }
                        //marche en cascade : si ne peux pas faire case 2 -> va à la case 1 ou 2
                        break;
                }


            }
            x = random.Next(1, 6);
            switch (x)
            {
                case 1: return RoomType.FILLER_1;
                case 2: return RoomType.FILLER_2;
                case 3: return RoomType.FILLER_3;
                case 4: return RoomType.FILLER_4;
                default: return RoomType.FILLER_5;
            }

        }




        /// <summary>
        /// set default description for ship
        /// </summary>
        /// <param name="ship"></param>
        /// <returns></returns>
        public static string DefaultDescription(Ship ship)
        {
            if (ship == null)
            {
                return "This is a strange one.";
            }
            return $"{ship.Name} : {ship.Stat.ToString()}";
        }
    }
}
