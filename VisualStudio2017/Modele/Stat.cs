﻿using System;

namespace Modele
{
    /// <summary>
    /// statistic class
    /// </summary>
    public class Stat : IEquatable<Stat>
    {
        public float Weight { get; set; }
        public float Speed { get; set; }
        public float Attack { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="weight">weight to set</param>
        /// <param name="speed">speed to set</param>
        /// <param name="attack">speed to set</param>
        public Stat(float weight, float speed, float attack)
        {
            if (weight < 0) weight = 0;
            Weight = weight;

            if (speed < 0) speed = 0;
            Speed = speed;

            if (attack < 0) attack = 0;
            Attack = attack;
        }

        /// <summary>
        /// ctor init 0,0,0
        /// </summary>
        public Stat() : this(0, 0, 0)
        { }


        public override string ToString()
        {
            return $"Weight : {Weight}, Speed :{Speed}, Attack :{Attack}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.GetType() == GetType()) return Equals(obj as Stat);
            return false;
        }

        public bool Equals(Stat other)
        {
            if (other == null) return false;
            if (other.Attack == Attack && other.Weight == Weight && other.Speed == Speed) return true;
            return false;
        }

        public override int GetHashCode()
        {
            return Weight.GetHashCode() + Speed.GetHashCode() + Attack.GetHashCode();
        }


    }
}
