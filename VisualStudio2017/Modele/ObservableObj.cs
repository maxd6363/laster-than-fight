﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Modele
{
    /// <summary>
    /// class for observable object
    /// </summary>
    public class ObservableObj : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        

        /// <summary>
        /// send PropertyCanged event
        /// </summary>
        /// <param name="propertyName">name of the property</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
