﻿using System;
using System.Diagnostics;
using System.IO;

namespace Modele
{
    public class PNJText : ObservableObj
    {
        public string currentText;
        public string CurrentText
        {
            get => currentText;
            private set
            {
                if(value != currentText)
                {
                    currentText = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private readonly Random random = new Random();

        public PNJText()
        {
            int nbSpeech = 0;
            try
            {
                using (StreamReader fileStreamName = new StreamReader(Directory.GetCurrentDirectory() + @".\..\..\src\text\Speech.dat"))
                {
                    try
                    {
                        nbSpeech = int.Parse(fileStreamName.ReadLine());
                        if (nbSpeech <= 0)
                        {
                            CurrentText = "Hey ! How are you ?";
                            //if an error occure
                        }
                    }
                    catch (ArgumentNullException) { Debug.WriteLine($"Null number of names"); }
                    catch (FormatException) { Debug.WriteLine($"Parse Error number of names"); }
                    catch (OverflowException) { Debug.WriteLine($"number of names overflow"); }

                    int x = random.Next(1, nbSpeech);
                    for (int i = 0; i < x; i++)
                    {
                        fileStreamName.ReadLine();
                    }
                    CurrentText = fileStreamName.ReadLine();
                }
            }
            catch (ArgumentException) { Debug.WriteLine("can't access SaveShip.dat (wrong path)"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (folder not found)"); }
            catch (FileNotFoundException) { Debug.WriteLine("can't access SaveShip.dat (file not found)"); }
            catch (IOException) { Debug.WriteLine("can't access SaveShip.dat (Input/Output Exception)"); }

        }

    }
}
