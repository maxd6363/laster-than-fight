﻿using System;

namespace Modele.Rooms.Filler
{
    /// <summary>
    /// RoomFiller class, extends Room, room with no cost to fill the ship to desire number of room
    /// </summary>
    public class RoomFiller : Room
    {

        public string RealName { get; private set; }

        /// <summary>
        /// ctor for RoomType, Name ,load Description and BitmapImage
        /// </summary>
        /// <param name="type">Type of room</param>
        public RoomFiller(RoomType type) : base()
        {
            if (!(type == RoomType.FILLER_1 || type == RoomType.FILLER_2 || type == RoomType.FILLER_3 || type == RoomType.FILLER_4 || type == RoomType.FILLER_5))
            {
                throw new ArgumentException("Illegal type argument");
            }
            Type = type;
            Name = type.ToString();

            SetDescription();
            base.LoadBitmapImage();
        }


        /// <summary>
        /// Set description based on Type
        /// </summary>
        private void SetDescription()
        {
            switch (Type)
            {
                case RoomType.FILLER_1:                 
                    RealName = "Botanic Room";
                    Description = "Potatoes ain't gonna grow themself";
                    ID = "10";
                    break;
                case RoomType.FILLER_2:
                    RealName = "Storage Room";
                    Description = "Inspect for unwanted alien\nlife form every two weeks.";
                   
                    ID = "11";
                    break;
                case RoomType.FILLER_3:
                    RealName = "Research room";
                    Description = "For like, science and stuff";
                    
                    ID = "12";
                    break;
                case RoomType.FILLER_4:
                    RealName = "Bedroom";
                    Description = "Nobody has time for this anyway";
                    
                    ID = "13";
                    break;
                case RoomType.FILLER_5:
                    RealName = "Corridor";
                    Description = "We somehow found some space to waste";
                  
                    ID = "15";
                    break;

            }
        }

    }
}
