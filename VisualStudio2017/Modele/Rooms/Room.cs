﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;
using Modele.Ressources;

namespace Modele.Rooms
{
    /// <summary>
    /// Room class
    /// </summary>
    public class Room : IEquatable<Room>, IRoom
    {
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public BitmapImage ImageRoom { get; protected set; }
        public Stat Stat { get; protected set; }
        public string ID { get; protected set; }
        public RoomType Type { get; protected set; }
        public Dictionary<Ressource, int> RoomCostDic { get; protected set; }

        /// <summary>
        /// Room constructor (initialize Type to RoomType.NONE & ID to "00")
        /// </summary>
        public Room()
        {
            Stat = new Stat();
            Type = RoomType.NONE;
            ID = "00";
            RoomCostDic = new Dictionary<Ressource, int>();

        }
        /// <summary>
        /// Loads BitmapImage of room based on Type
        /// </summary>
        protected void LoadBitmapImage()
        {
            try
            {

                RoomImage.DicoRoomBmp.TryGetValue(Type.ToString().ToUpper(), out BitmapImage bmp);
                if (bmp != null)
                    ImageRoom = bmp;
            }
            catch (DirectoryNotFoundException) { Debug.WriteLine("Can't load Room image (folder)"); }
            catch (FileNotFoundException) { Debug.WriteLine("Can't load Room image (file)"); }
            catch (WebException) { Debug.WriteLine("Can't load Room image (tamer)"); }
        }




        /// <summary>
        /// Returns a string made from the Name and the toString of his Type
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Name} {Type.ToString()}";
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj">Object to be tested</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.GetType() == GetType()) return Equals(obj as Room);
            return false;
        }


        public bool Equals(Room other)
        {
            if (other == null) { return false; }

            if (Name == other.Name && ID == other.ID && Description == other.Description)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() * ID.GetHashCode() * Description.GetHashCode();
        }
    }
}
