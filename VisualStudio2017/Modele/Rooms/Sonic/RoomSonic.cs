﻿using Modele.Ressources.Type.Electronic;
using Modele.Ressources.Type.Energy;
using Modele.Ressources.Type.Metal;
using System;

namespace Modele.Rooms.Sonic
{
    /// <summary>
    /// RoomSonic class, extends Room
    /// </summary>
    public class RoomSonic : Room
    {
        /// <summary>
        /// ctor for RoomType, Name ,load Description and BitmapImage
        /// </summary>
        /// <param name="type">Type of room</param>
        public RoomSonic(RoomType type) : base()
        {
            if (!(type == RoomType.SONIC_FUEL || type == RoomType.SONIC_ION || type == RoomType.SONIC_NUCLEAR))
            {
                throw new ArgumentException("Illegal type argument");
            }


            Type = type;
            Name = type.ToString();
            SetDescription();
            Stat = new Stat(1, 10, 0);
            base.LoadBitmapImage();


        }

        /// <summary>
        /// Set description based on Type
        /// </summary>
        private void SetDescription()
        {
            switch (Type)
            {
                case RoomType.SONIC_FUEL: // Cost : Scraps + CopperWire + Fuel
                    Description = "Regular and powerful\nfuel reactor.";
                    ID = "20";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.SCRAPS), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.COPPER_WIRE), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.FUEL), 10);
                    break;

                case RoomType.SONIC_ION: // Cost : Mirridium + Rotor + IonGen
                    Description = "A very expensive, very efficient,\nvery dangerous thruster. \nAlso has a microwave fonction.";
                    ID = "21";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.MIRRIDIUM), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.ROTOR), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.ION_GENERATOR), 10);
                    break;

                case RoomType.SONIC_NUCLEAR: // Cost : IronPlates + PrintedCircuits + NuclearRod
                    Description = "Your now three armed\nmechanic loves it.\nMaybe don't get too close.";
                    ID = "22";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.IRON), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.PRINTED_CIRCUITS), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.NUCLEAR_ROD), 10);
                    break;

            }
        }

    }
}
