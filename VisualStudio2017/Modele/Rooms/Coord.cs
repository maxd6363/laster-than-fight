﻿using System;

namespace Modele.Rooms
{
    /// <summary>
    /// class containing coordonate of room
    /// </summary>
    public class Coord : IEquatable<Coord>
    {
        public int CoordX { get; set; }
        public int CoordY { get; set; }

        /// <summary>
        /// ctor for Coord
        /// </summary>
        /// <param name="x">X postion in tab</param>
        /// <param name="y">Y postion in tab</param>
        public Coord(int x, int y)
        {
            this.CoordX = x;
            this.CoordY = y;

        }
        /// <summary>
        /// ctor for Coord, set to 0 - 0
        /// </summary>
        public Coord() : this(0, 0)
        { }



        public override string ToString()
        {
            return $"X : {CoordX}\t Y : {CoordY}";
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.GetType() == GetType()) return Equals(obj as Coord);
            return false;
        }
        public bool Equals(Coord other)
        {
            if (other == null) return false;
            if (other.CoordX == CoordX && other.CoordY == CoordY) return true;
            return false;
        }

        public override int GetHashCode()
        {
            return CoordX.GetHashCode() * CoordY.GetHashCode();
        }


    }
}
