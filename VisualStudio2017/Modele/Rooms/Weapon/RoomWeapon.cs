﻿using Modele.Ressources.Type.Energy;
using Modele.Ressources.Type.Metal;
using Modele.Ressources.Type.Electronic;
using System;

namespace Modele.Rooms.Weapon
{
    /// <summary>
    /// RoomWeapon class, extends Room
    /// </summary>
    public class RoomWeapon : Room
    {
        private Random Random { get; } = new Random();

        /// <summary>
        /// ctor for RoomType, Name ,load Description and BitmapImage
        /// </summary>
        /// <param name="type">Type of room</param>
        public RoomWeapon(RoomType type) : base()
        {
            if (!(type == RoomType.WEAPON_CANNON || type == RoomType.WEAPON_FLAK || type == RoomType.WEAPON_LASER))
            {
                throw new ArgumentException("Illegal type argument");
            }

            Type = type;
            Name = type.ToString();
            Stat = new Stat(15, 0, 25);
            SetDescription();
            base.LoadBitmapImage();


            if(type == RoomType.WEAPON_CANNON)
            {
                if(RoomImage.ListCannon.Count != 0)
                {
                    //if some image are loaded 
                    ImageRoom = RoomImage.ListCannon[Random.Next(0, RoomImage.ListCannon.Count)];
                }
            }



        }

        /// <summary>
        /// Set description based on Type
        /// </summary>
        private void SetDescription()
        {
            switch (Type)
            {
                case RoomType.WEAPON_CANNON: // Cost : IronPlates + CopperWire + Fuel
                    Description = "A simple yet effective cannon.\nShoot first, ask questions later.";
                    ID = "40";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.IRON), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.COPPER_WIRE), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.FUEL), 10);
                    break;


                case RoomType.WEAPON_FLAK: // Cost : Scraps + Rotor + NuclearRod
                    Description = "Why would you aim when\nyou could just use a flak gun ?";
                    ID = "41";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.SCRAPS), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.ROTOR), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.NUCLEAR_ROD), 10);
                    break;
                case RoomType.WEAPON_LASER: // Cost : Mirridium + PrintedCir + IonGen
                    Description = "The power of light can saw entire ships in half.\nNothing religious tho";
                    ID = "42";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.MIRRIDIUM), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.PRINTED_CIRCUITS), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.ION_GENERATOR), 10);
                    break;

            }
        }
    }
}
