﻿namespace Modele.Rooms
{
    /// <summary>
    /// Enumeration : all types of constructible rooms. The commentary represent the hexadecimal value for saving and loading
    /// </summary>
    public enum RoomType
    {
       /*0*/NONE,

       /*1*/SONIC_ION,
       /*2*/SONIC_FUEL,
       /*3*/SONIC_NUCLEAR,

       /*4*/TECH_HACKING,
       /*5*/TECH_SHIELD,
       /*6*/TECH_CLOAKING,

       /*7*/WEAPON_CANNON,
       /*8*/WEAPON_FLAK,
       /*9*/WEAPON_LASER,

       /*A*/FILLER_1,
       /*B*/FILLER_2,
       /*C*/FILLER_3,
       /*D*/FILLER_4,
       /*E*/FILLER_5
    }
}
