﻿using Modele.Ressources.Type.Electronic;
using Modele.Ressources.Type.Energy;
using Modele.Ressources.Type.Metal;
using System;

namespace Modele.Rooms.Tech
{
    /// <summary>
    /// RoomTech class, extends Room
    /// </summary>
    public class RoomTech : Room
    {
        /// <summary>
        /// ctor for RoomType, Name ,load Description and BitmapImage
        /// </summary>
        /// <param name="type">Type of room</param>
        public RoomTech(RoomType type) : base()
        {
            if (!(type == RoomType.TECH_CLOAKING || type == RoomType.TECH_HACKING || type == RoomType.TECH_SHIELD))
            {
                throw new ArgumentException("Illegal type argument");
            }


            Type = type;
            Name = type.ToString();
            SetDescription();
            Stat = new Stat(20, 1, 10);
            base.LoadBitmapImage();


        }
        /// <summary>
        /// Set description based on Type
        /// </summary>
        private void SetDescription()
        {
            switch (Type)
            {
                case RoomType.TECH_CLOAKING: // Cost : Mirridium + CopperWire + NuclearRod
                    Description = "Don't spy on the\ngirls' restroom";
                    ID = "30";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.MIRRIDIUM), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.COPPER_WIRE), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.NUCLEAR_ROD), 10);
                    break;

                case RoomType.TECH_HACKING: // Cost : IronPlates + PrintedCir + IonGen
                    Description = "Replace their deadly weapons\ncontrols with cute kitten gifs.";
                    ID = "31";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.IRON), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.PRINTED_CIRCUITS), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.ION_GENERATOR), 10);
                    break;

                case RoomType.TECH_SHIELD: // Cost : Scraps + Rotor + Fuel
                    Description = "A bunch of scraps flex-tapped together,\nrotating to protect your ship against projectiles and debris.\n\"Shield\" is rather ambitious.";
                    ID = "32";
                    RoomCostDic.Add(new RessourceMetal(RessourceMetalType.SCRAPS), 10);
                    RoomCostDic.Add(new RessourceElectronic(RessourceElectronicType.ROTOR), 10);
                    RoomCostDic.Add(new RessourceEnergy(RessourceEnergyType.FUEL), 10);
                    break;

            }
        }
    }
}
