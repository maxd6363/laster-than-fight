﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;

namespace Modele.Rooms
{
    /// <summary>
    /// Static class containing BitmapImage of room (increase performence for ship display)
    /// </summary>
    public static class RoomImage
    {
        public static Dictionary<string, BitmapImage> DicoRoomBmp { get; private set; }
        public static List<BitmapImage> ListCannon { get; private set; }
        internal static string Path { get; set; }


        /// <summary>
        /// Static constructor
        /// </summary>
        static RoomImage()
        {
            DicoRoomBmp = new Dictionary<string, BitmapImage>();
            ListCannon = new List<BitmapImage>();
            Path = Directory.GetCurrentDirectory() + @".\..\..\src\img\Rooms\";

            LoadBitmapImage();
            LoadCannonImage();
        }


        /// <summary>
        /// Loads every Image of resources in Dictionary
        /// </summary>
        private static void LoadBitmapImage()
        {
            foreach (RoomType rt in Enum.GetValues(typeof(RoomType)))
            {
                try
                {
                    if (rt == RoomType.NONE)
                    {
                        DicoRoomBmp.Add(rt.ToString().ToUpper(), null);
                    }
                    else
                    {
                        DicoRoomBmp.Add(rt.ToString().ToUpper(), new BitmapImage(new Uri(Path + rt.ToString().ToUpper() + ".png")));
                    }
                }
                catch (Exception e) when (e is DirectoryNotFoundException || e is FileNotFoundException || e is WebException)
                {
                    //Image not found
                    try
                    {
                        DicoRoomBmp.Add(rt.ToString().ToUpper(), new BitmapImage(new Uri(Path + "../NAN.png")));
                    }
                    catch (DirectoryNotFoundException)
                    {
                        Debug.WriteLine("Can't load NAN.png (folder)");
                        DicoRoomBmp.Add(rt.ToString().ToUpper(), null);
                    }
                    catch (FileNotFoundException)
                    {
                        Debug.WriteLine("Can't load NAN.png (file)");
                        DicoRoomBmp.Add(rt.ToString().ToUpper(), null);
                    }
                }
            }
        }


        /// <summary>
        /// Loads every Image of resources in Dictionary
        /// </summary>
        private static void LoadCannonImage()
        {

            try
            {

                ListCannon.Add(new BitmapImage(new Uri(Path + "WEAPON_CANNON1.png")));
                ListCannon.Add(new BitmapImage(new Uri(Path + "WEAPON_CANNON2.png")));
                ListCannon.Add(new BitmapImage(new Uri(Path + "WEAPON_CANNON3.png")));
                ListCannon.Add(new BitmapImage(new Uri(Path + "WEAPON_CANNON4.png")));
                ListCannon.Add(new BitmapImage(new Uri(Path + "WEAPON_CANNON5.png")));

            }
            catch (Exception e) when (e is DirectoryNotFoundException || e is FileNotFoundException || e is WebException)
            {
                //Image not found
                try
                {
                    ListCannon.Add(new BitmapImage(new Uri(Path + "../NAN.png")));
                }
                catch (DirectoryNotFoundException) { Debug.WriteLine("Can't load NAN.png (folder)"); }
                catch (FileNotFoundException) { Debug.WriteLine("Can't load NAN.png (file)"); }
            }
        }




    }
}
