﻿using System.Windows.Media.Imaging;

namespace Modele.Rooms
{
    public interface IRoom
    {


        string Name { get; }
        string Description { get; }
        BitmapImage ImageRoom { get; }

    }
}
