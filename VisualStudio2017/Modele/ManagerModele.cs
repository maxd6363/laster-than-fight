﻿using Modele.Data;
using Modele.Ressources;
using Modele.Rooms;
using Modele.Ships;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Modele
{
    /// <summary>
    /// main manager for Modele
    /// </summary>
    public class ManagerModele
    {
        private ILoad Loader { get; set; }
        private ISave Saver { get; set; }

        public static ObservableCollection<Ship> ListShip { get; private set; }
        public static List<Ressource> ListRessource { get; private set; }
        public static ReadOnlyCollection<IRoom> ListRoom { get; private set; }

        private static Ship currentShip;
        public static Ship CurrentShip
        {
            get
            {
                return currentShip;
            }
            set
            {
                if (value != currentShip)
                {
                    currentShip = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public static event PropertyChangedEventHandler PropertyChanged;

        public static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(null, new PropertyChangedEventArgs(propertyName));
        }



        /// <summary>
        /// ctor for Manager, dependency injection for ILoad and ISave
        /// </summary>
        /// <param name="loader">ILoad to use</param>
        /// <param name="saver">ISave to use</param>
        public ManagerModele(ILoad loader, ISave saver)
        {
            Loader = loader ?? throw new ArgumentNullException(nameof(loader));
            Saver = saver ?? throw new ArgumentNullException(nameof(saver));

            Load();

        }



        /// <summary>
        /// load every information using ILoad
        /// </summary>
        public void Load()
        {
            ListShip = new ObservableCollection<Ship>(Loader.LoadListShip());
            ListRessource = Loader.LoadListRessource();
            ListRoom = new ReadOnlyCollection<IRoom>(Loader.LoadListRoom());

        }

        /// <summary>
        /// save every information using ISave
        /// </summary>
        public void Save()
        {
            SaveShips();
            SaveRessources();
            SaveRooms();
        }

        /// <summary>
        /// save ships using ISave
        /// </summary>
        public void SaveShips()
        {
            Saver.SaveListShip(ListShip.ToList());
        }


        /// <summary>
        /// save ressources using ISave
        /// </summary>
        public void SaveRessources()
        {
            Saver.SaveListRessource(ListRessource.ToList());
        }

        /// <summary>
        /// save rooms using ISave
        /// </summary>
        public void SaveRooms()
        {
            Saver.SaveListRoom(ListRoom.ToList());
        }



        /// <summary>
        /// generate a ship and set in the CurrentShip property
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="nbRoom">number of rooms</param>
        /// <param name="type">Type (ShipType)</param>
        /// <param name="percentFiller">percentage of filler room (if possible with ressources available) </param>
        /// <returns>Ship generated</returns>
        public static void MakeAShip(string name, int nbRoom, ShipType type, int percentFiller)
        {
            CurrentShip=ShipFactory.MakeAShip(name, nbRoom, type, percentFiller);
        }




        /// <summary>
        /// Add a ship to the ObservableCOllection of ship
        /// </summary>
        /// <param name="s">ship to add</param>
        public static void AddShip()
        {
            if (!ListShip.Contains(CurrentShip) && CurrentShip != null)
            {
                ListShip.Add(CurrentShip);
            }
        }

        /// <summary>
        /// remove a ship from the observavle collection of ship
        /// </summary>
        /// <param name="s">ship to remove</param>
        public static void DeleteShip(Ship s)
        {
            if (ListShip.Contains(s))
            {
                ListShip.Remove(s);
            }
            else
            {
                Debug.WriteLine("Can't delete ship");
            }
        }


        /// <summary>
        /// add ressource to r.NbRessourcePlayer
        /// </summary>
        /// <param name="r">ressource to modify</param>
        /// <param name="qte">quantity to add (or remove if negative)</param>
        public static void AddRessource(Ressource r, int qte)
        {
            if (r == null)
            {
                Debug.WriteLine("Ressource null");
                return;
            }
            ListRessource.ToList().Find(res => res.Equals(r)).AddRessource(qte);
            NotifyPropertyChanged();
        }




    }
}
