﻿using Modele.Ressources;
using Modele.Rooms;
using Modele.Ships;
using System.Collections.Generic;

namespace Modele.Data
{

    /// <summary>
    /// Interface for persistence of Ressource, Ship, Room (Saving)
    /// </summary>
    public interface ISave
    {
        void SaveListRessource(List<Ressource> listRessource);
        void SaveListShip(List<Ship> listShip);
        void SaveListRoom(List<IRoom> listRoom);
    }
}
