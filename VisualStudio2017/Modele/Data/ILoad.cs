﻿using Modele.Ressources;
using Modele.Rooms;
using Modele.Ships;
using System.Collections.Generic;

namespace Modele.Data
{
    /// <summary>
    /// Interface for persistence of Ressource, Ship, Room (Loading)
    /// </summary>
    public interface ILoad
    {
        List<Ressource> LoadListRessource();
        List<Ship> LoadListShip();
        List<IRoom> LoadListRoom();

    }
}
