﻿using System;

namespace DynamicBackground
{
    /// <summary>
    /// utility math class 
    /// </summary>
    internal static class GeometryCalc
    {
        /// <summary>
        /// Euclidian Distance between 2 points
        /// </summary>
        /// <param name="a">Point A</param>
        /// <param name="b">Point B</param>
        /// <returns>Euclidian Distance between Point A and Point B</returns>
        internal static double EuclidianDistance(Ptn a, Ptn b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        /// <summary>
        /// Distance on X axis between 2 points
        /// </summary>
        /// <param name="a">Point A</param>
        /// <param name="b">Point B</param>
        /// <returns>Distance on X axis between Point A and Point B</returns>
        internal static double XDistance(Ptn a, Ptn b)
        {
            return a.X - b.X;
        }

        /// <summary>
        /// Distance on Y axis between 2 points
        /// </summary>
        /// <param name="a">Point A</param>
        /// <param name="b">Point B</param>
        /// <returns>Distance on Y axis between Point A and Point B</returns>
        internal static double YDistance(Ptn a, Ptn b)
        {
            return a.Y - b.Y;
        }
    }
}
