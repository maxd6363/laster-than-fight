﻿using DynamicBackground.Properties;

namespace DynamicBackground
{
    /// <summary>
    /// manager for the dynamique background
    /// - Acrive state
    /// </summary>
    public static class BackgroundManager
    {
        public static bool Active
        {
            get => Settings.Default.backgroundActive;
        }


        /// <summary>
        /// invert Active and save to Settings
        /// </summary>
        public static void ChangeState()
        {

            Settings.Default.backgroundActive = !Settings.Default.backgroundActive;
            Settings.Default.Save();

        }



    }
}
