﻿using System;
using System.Windows;
using System.Windows.Input;

namespace DynamicBackground
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private double refresh;
        public double Refresh
        {
            get => refresh;
            set
            {
                if (value <= 0) return;

                if (value != refresh)
                {
                    refresh = value;
                }
            }
        }
        public int Hook { get; set; }
        public int NbParticle { get; set; }
        public int Thikness { get; set; }




        public MainWindow()
        {
            Background b = new Background();
            Refresh = 1000 / b.refresh;
            Hook = b.hook;
            NbParticle = b.nbParticle;
            Thikness = b.thikness;

            KeyDown += MainWindow_KeyDown;

            InitializeComponent();
            DataContext = this;
            MyFrame.NavigationService.Navigate(b);
        }

        private void MainWindow_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Update();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }


        void Update()
        {
            MyFrame.NavigationService.Navigate(new Background
            {
                refresh = (int)(1000 / this.Refresh),
                hook = this.Hook,
                nbParticle = this.NbParticle,
                thikness = this.Thikness
            }); ;
        }
    }
}
