﻿using System.Windows.Shapes;

namespace DynamicBackground
{
    /// <summary>
    /// Interface for Particle
    /// every particule needs : 
    /// - Update(); (position)
    /// - Form (shape of paticule)
    /// - Pos (Position on screen)
    /// - Size (Size of particule)
    /// - Gravity() (gravity relation between 2 IParticule)
    /// </summary>
    internal interface IParticle
    {
        void Update();
        Shape Form { get; }
        Ptn Pos { get; }
        int Size { get; }

        void Gavity(IParticle pt);


    }
}
