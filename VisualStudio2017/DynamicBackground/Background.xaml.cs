﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DynamicBackground
{
    /// <summary>
    /// Interaction logic for Background.xaml
    /// </summary>

    public partial class Background : Page
    {
        internal int refresh = 15;
        private readonly Random random;
        private Color color;
        internal int hook = 100;
        internal int nbParticle = 20;
        internal int thikness = 3;

        public Background()
        {

            int h;
            int w;

            random = new Random();


            InitializeComponent();
            MyCanvas.Background = new SolidColorBrush(Color.FromArgb(0, 44, 62, 80));
            //color = Color.FromArgb(0, 62, 138, 226);
            color = Color.FromArgb(200, 44, 62, 80);

            Loaded += delegate
            {
                h = (int)Application.Current.MainWindow.ActualHeight;
                w = (int)Application.Current.MainWindow.ActualWidth;
                List<ParticlePoint> particles = Enumerable.Range(0, nbParticle).Select(element => new ParticlePoint(random, h, w)).ToList();
                Draw(particles);

            };


        }

        private async void Draw(List<ParticlePoint> listParticles)
        {
            while (true)
            {



                foreach (IParticle particle in listParticles)
                {
                    particle.Update();

                    Canvas.SetLeft(particle.Form, particle.Pos.X);
                    Canvas.SetTop(particle.Form, particle.Pos.Y);
                    MyCanvas.Children.Add(particle.Form);

                    foreach (IParticle neighbor in listParticles)
                    {

                        double distance = GeometryCalc.EuclidianDistance(particle.Pos, neighbor.Pos);

                        if (distance > hook || particle == neighbor)
                        {
                            //particle too far away to hook up
                            continue;
                        }

                        particle.Gavity((ParticlePoint)neighbor);

                        //adjust for center line to middle of particule
                        int adjustParticule = particle.Size / 2;
                        int adjustNeighbor = neighbor.Size / 2;

                        //set alpha to line
                        color.A = (byte)(255 - distance / hook * 255);

                        Line line = new Line
                        {
                            X1 = particle.Pos.X + adjustParticule,
                            X2 = neighbor.Pos.X + adjustNeighbor,
                            Y1 = particle.Pos.Y + adjustParticule,
                            Y2 = neighbor.Pos.Y + adjustNeighbor,
                            Stroke = new SolidColorBrush(color),
                            StrokeThickness = thikness
                        };
                        MyCanvas.Children.Add(line);

                        Ellipse ellipse = new Ellipse();
                    }
                }
                await Task.Delay(refresh);
                MyCanvas.Children.Clear();
            }
        }


    }
}
