﻿namespace DynamicBackground
{
    /// <summary>
    /// class representing a point on screen
    /// </summary>
    internal class Ptn
    {
        internal double X { get; set; }
        internal double Y { get; set; }

        /// <summary>
        /// ctor for Ptn
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        public Ptn(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
        /// <summary>
        /// ctor with 0 - 0 position
        /// </summary>
        public Ptn() : this(0, 0)
        { }
    }
}
