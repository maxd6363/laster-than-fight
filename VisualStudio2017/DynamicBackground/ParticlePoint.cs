﻿using System;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DynamicBackground
{

    internal class ParticlePoint : IParticle
    {
        public Ptn Pos { get; private set; }
        public Ptn Velocity { get; private set; }
        public int Size { get; private set; }
        public Shape Form { get; private set; }

        private readonly int frameHeight;
        private readonly int frameWidth;

        private readonly Random random;


        public ParticlePoint(Random random, int h, int w)
        {
            this.frameHeight = h;
            this.frameWidth = w;
            this.random = random;
            Size = random.Next(5, 10);

            Pos = new Ptn(random.Next(Size, frameHeight - Size), random.Next(Size, frameWidth - Size));
            Velocity = new Ptn(random.NextDouble() * 10 - 5, random.NextDouble() * 10 - 5);




            Form = new Ellipse
            {
                Fill = new SolidColorBrush(Color.FromArgb(200, 104, 122, 140)),
                StrokeThickness = Size,
                Width = Size,
                Height = Size
            };
        }

        public void Update()
        {


            Pos.X += Velocity.X;
            Pos.Y += Velocity.Y;

            if (Pos.X > frameWidth - Size || Pos.X < Size)
            {
                Velocity.X *= -1;
            }

            if (Pos.Y > frameHeight - Size || Pos.Y < Size)
            {
                Velocity.Y *= -1;
            }
        }

        public void Gavity(IParticle pt)
        {
            Velocity.X -= (GeometryCalc.XDistance(this.Pos, pt.Pos) / 300);
            Velocity.Y -= (GeometryCalc.YDistance(this.Pos, pt.Pos) / 300);
        }

    }
}
