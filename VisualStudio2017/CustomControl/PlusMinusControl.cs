﻿using Modele;
using Sound;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace CustomControl
{
    /// <summary>
    /// custom control for + / - value
    /// </summary>
    public class PlusMinusControl : Control
    {
        public static readonly DependencyProperty ValueProperty;
        public static readonly DependencyProperty NbToAddProperty;
        public static readonly DependencyProperty AutoSaveProperty;
        public static readonly DependencyProperty ManagerProperty;
        public int Value
        {
            get
            {
                return (int)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }



        public int NbToAdd
        {
            get
            {
                return (int)GetValue(NbToAddProperty);
            }
            set
            {
                if (value > 0)
                {
                    SetValue(NbToAddProperty, value);
                }
                else
                {
                    Debug.WriteLine("NbToAdd can't be negative");
                }
            }
        }

        public bool AutoSave
        {
            get => (bool)GetValue(AutoSaveProperty);
            set => SetValue(AutoSaveProperty, value);
        }

        public ManagerModele Manager
        {
            get => (ManagerModele)GetValue(ManagerProperty);
            set => SetValue(ManagerProperty, value);
        }

        /// <summary>
        /// static ctor PlusMinusControl : register ValueProperty
        /// </summary>
        static PlusMinusControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PlusMinusControl), new FrameworkPropertyMetadata(typeof(PlusMinusControl)));

            ValueProperty = DependencyProperty.Register("Value", typeof(int), typeof(PlusMinusControl));
            NbToAddProperty = DependencyProperty.Register("NbToAdd", typeof(int), typeof(PlusMinusControl));
            AutoSaveProperty = DependencyProperty.Register("AutoSave", typeof(bool), typeof(PlusMinusControl));
            ManagerProperty = DependencyProperty.Register("Manager", typeof(ManagerModele), typeof(PlusMinusControl));
        }

        /// <summary>
        /// ctor PlusMinusControl
        /// </summary>
        public PlusMinusControl()
        {
            DataContext = this;
        }

        /// <summary>
        /// override OnApplyTemplate to add button event
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (!(GetTemplateChild("PlusBtn") is Button PlusBtn))
            {
                return;
            }
            if (!(GetTemplateChild("MinusBtn") is Button MinusBtn))
            {
                return;
            }

            PlusBtn.Click += PlusBtn_Click;
            MinusBtn.Click += MinusBtn_Click;



        }

        /// <summary>
        /// minus button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinusBtn_Click(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();
            Value -= NbToAdd;
            if (AutoSave)
            {
                Manager.SaveRessources();
            }

        }

        /// <summary>
        /// plus button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlusBtn_Click(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();
            Value += NbToAdd;
            Console.WriteLine(AutoSave);

            if (AutoSave)
            {
                Manager.SaveRessources();
            }
        }

    }
}
