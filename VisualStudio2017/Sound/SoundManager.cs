﻿using Sound.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Media;


namespace Sound
{
    /// <summary>
    /// Static Manager for sound and music
    /// </summary>
    public static class SoundManager
    {
        private static readonly MediaPlayer mediaPlayerSound;
        private static readonly MediaPlayer mediaPlayerMusic;
        private static readonly MediaPlayer mediaPlayerNarator;

        private static readonly List<string> listMusic;
        private static string lastMusicPlayed;
        private static readonly Random random;

        public static event EventHandler<MusicChangedEventArgs> MusicChanged;
        public static string CurrentMusicName { get; private set; }
        public static double MusicVolume { get; private set; }
        public static double SoundVolume { get; private set; }

        /// <summary>
        /// Static ctor, register the MediaEnded event and load sound/music level
        /// </summary>
        static SoundManager()
        {
            mediaPlayerSound = new MediaPlayer();
            mediaPlayerMusic = new MediaPlayer();
            mediaPlayerNarator = new MediaPlayer();
            mediaPlayerMusic.MediaEnded += MediaPlayerMusic_MediaEnded;

            listMusic = new List<string>();
            random = new Random();

            MusicVolume = Settings.Default.musicVolume;
            SoundVolume = Settings.Default.soundVolume;

            mediaPlayerMusic.Volume = MusicVolume;
            mediaPlayerSound.Volume = SoundVolume;
            mediaPlayerNarator.Volume = SoundVolume;

            InitListMusic();

        }

        /// <summary>
        /// Set the current volume of music
        /// </summary>
        /// <param name="vol">Volume to set (0~1) </param>
        public static void SetMusicVolume(double vol)
        {
            if (vol < 0) MusicVolume = 0.0;
            if (vol > 1) MusicVolume = 1.0;
            MusicVolume = vol;

            mediaPlayerMusic.Volume = MusicVolume;
            Settings.Default.musicVolume = MusicVolume;
            Settings.Default.Save();
        }

        /// <summary>
        /// Set the current volume of sound
        /// </summary>
        /// <param name="vol">volume to set (0~1) </param>
        public static void SetSoundVolume(double vol)
        {
            if (vol < 0) SoundVolume = 0.0;
            if (vol > 1) SoundVolume = 1.0;
            SoundVolume = vol;

            mediaPlayerSound.Volume = SoundVolume;
            Settings.Default.soundVolume = SoundVolume;
            Settings.Default.Save();
        }


        /// <summary>
        /// Main methode to play sound
        /// </summary>
        /// <param name="fileName">Name of mp3 file to play</param>
        private static void PlaySound(string fileName)
        {
            try
            {
                mediaPlayerSound.Open(new Uri(Directory.GetCurrentDirectory() + $@".\..\..\src\sound\{fileName}.mp3"));
                mediaPlayerSound.Play();


            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine($"Can't load {fileName}.mp3");
            }
            catch (UriFormatException)
            {
                Debug.WriteLine($"Can't load {fileName}.mp3");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Accessibility state getter
        /// </summary>
        /// <returns>State</returns>
        public static bool IsNaratorActive()
        {
            return Settings.Default.accessibility;
        }

        /// <summary>
        /// Accessibility state setter
        /// </summary>
        /// <param name="setter">State</param>
        public static void SetNarator(bool setter)
        {
            Settings.Default.accessibility = setter;
            Settings.Default.Save();
        }

        /// <summary>
        /// Play GO sound
        /// </summary>
        public static void GoSound() => PlaySound("GO_SOUND");

        /// <summary>
        /// Play BACK sound
        /// </summary>
        public static void BackSound() => PlaySound("BACK_SOUND");

        /// <summary>
        /// Play EVENT sound
        /// </summary>
        public static void EventSound() => PlaySound("EVENT_SOUND");

        /// <summary>
        /// Play STARTUPSOUND sound
        /// </summary>
        public static void StartUpSound() => PlaySound("STARTUP_SOUND");


        /// <summary>
        /// Play CHECK sound
        /// </summary>
        public static void CheckSound() => PlaySound("CHECK_SOUND");

        /// <summary>
        /// Play CLICK sound
        /// </summary>
        public static void ClickSound() => PlaySound("CLICK_SOUND");

        /// <summary>
        /// Play DELETE sound
        /// </summary>
        public static void DeleteSound() => PlaySound("DELETE_SOUND");


        /*--------------------------------------NARATOR---------------------------------*/

        /// <summary>
        /// play narator sound on mediaPlayer
        /// </summary>
        /// <param name="fileName"></param>
        private static void PlayNarator(string fileName)
        {
            try
            {
                mediaPlayerNarator.Open(new Uri(Directory.GetCurrentDirectory() + $@".\..\..\src\sound\{fileName}.mp3"));
                mediaPlayerNarator.Play();


            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine($"Can't load {fileName}.mp3");
            }
            catch (UriFormatException)
            {
                Debug.WriteLine($"Can't load {fileName}.mp3");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }




        /// <summary>
        /// Play "PLAY" sound for hover event
        /// </summary>
        public static void PlayHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/PLAY_HOVER");
        }

        /// <summary>
        /// Play "EXIT" sound for hover event
        /// </summary>
        public static void ExitHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/EXIT_HOVER");
        }

        /// <summary>
        /// Play "SETTINGS" sound for hover event
        /// </summary>
        public static void SettingsHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/SETTINGS_HOVER");
        }

        /// <summary>
        /// Play "ACCESSIBILITY" sound for hover event
        /// </summary>
        public static void AccessibilityHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/ACCESSIBILITY_HOVER");
        }


        /// <summary>
        /// Play "BACK" sound for hover event
        /// </summary>
        public static void BackHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/BACK_HOVER");
        }

        /// <summary>
        /// Play "BACKGROUND" sound for hover event
        /// </summary>
        public static void BackgroundHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/BACKGROUND_HOVER");
        }

        /// <summary>
        /// Play "CONTRAST" sound for hover event
        /// </summary>
        public static void ContrastHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/CONTRAST_HOVER");
        }

        /// <summary>
        /// Play "DELETE" sound for hover event
        /// </summary>
        public static void DeleteHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/DELETE_HOVER");
        }

        /// <summary>
        /// Play "FULLSCREEN" sound for hover event
        /// </summary>
        public static void FullscreenHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/FULLSCREEN_HOVER");
        }

        /// <summary>
        /// Play "GENERATOR" sound for hover event
        /// </summary>
        public static void GeneratorHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/GENERATOR_HOVER");
        }

        /// <summary>
        /// Play "GENERATE" sound for hover event
        /// </summary>
        public static void GenerateHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/GENERATE_HOVER");
        }

        /// <summary>
        /// Play "SAVE" sound for hover event
        /// </summary>
        public static void SaveHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/SAVE_HOVER");
        }

        /// <summary>
        /// Play "Discard" sound for hover event
        /// </summary>
        public static void DiscardHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/DISCARD_HOVER");
        }

        /// <summary>
        /// Play "HANGAR" sound for hover event
        /// </summary>
        public static void HangarHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/HANGAR_HOVER");
        }

        /// <summary>
        /// Play "LOAD" sound for hover event
        /// </summary>
        public static void LoadHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/LOAD_HOVER");
        }

        /// <summary>
        /// Play "MINUS" sound for hover event
        /// </summary>
        public static void MinusHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/MINUS_HOVER");
        }

        /// <summary>
        /// Play "NARRATOR" sound for hover event
        /// </summary>
        public static void NarratorHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/NARRATOR_HOVER");
        }

        /// <summary>
        /// Play "PLUS" sound for hover event
        /// </summary>
        public static void PlusHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/PLUS_HOVER");
        }

        /// <summary>
        /// Play "RESOURCE" sound for hover event
        /// </summary>
        public static void ResourceHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/RESOURCE_HOVER");
        }

        /// <summary>
        /// Play "ROOM" sound for hover event
        /// </summary>
        public static void RoomHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/ROOM_HOVER");
        }


        /// <summary>
        /// Play "SPEED" sound for hover event
        /// </summary>
        public static void SpeedHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/SPEED_HOVER");
        }

        /// <summary>
        /// Play "TECH" sound for hover event
        /// </summary>
        public static void TechHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/TECH_HOVER");
        }

        /// <summary>
        /// Play "WEAPON" sound for hover event
        /// </summary>
        public static void WeaponHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/WEAPON_HOVER");
        }

        /// <summary>
        /// Play "EVERYTHING" sound for hover event
        /// </summary>
        public static void EverythingHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/EVERYTHING_HOVER");
        }




        /// <summary>
        /// Play "EVERYTHING" sound for hover event
        /// </summary>
        public static void AutoSaveHover()
        {
            if (IsNaratorActive())
                PlayNarator("NARATOR/AUTO_SAVE_HOVER");
        }





        /*--------------------------END NARATOR------------------------------*/



        /// <summary>
        /// Play "PASS" sound for Nicomete
        /// </summary>
        public static void PassNicomete() => PlaySound("PASS" + random.Next(1, 6).ToString());




        /*-------------------------------VOICE------------------------------------*/

        /// <summary>
        /// Play VOICE LINE "SPEED" sound for Nicomete
        /// </summary>
        public static void VoiceSpeed() => PlaySound("SPEED");

        /// <summary>
        /// Play VOICE LINE "TECH" sound for Nicomete
        /// </summary>
        public static void VoiceTech() => PlaySound("TECH");

        /// <summary>
        /// Play VOICE LINE "WEAPON" sound for Nicomete
        /// </summary>
        public static void VoiceWeapon() => PlaySound("WEAPON");
        /// <summary>
        /// Play VOICE LINE "EVERYTHING" sound for Nicomete
        /// </summary>
        public static void VoiceEverything() => PlaySound("EVERYTHING");

        /// <summary>
        /// Play "PASS" sound for Nicomete
        /// </summary>
        public static void StartApp() => PlaySound("START" + random.Next(1, 8).ToString());

        /*-------------------------------END VOICE--------------------------------*/













        /// <summary>
        /// load list of music with .mp3 in the folder src/music, user can add his own music
        /// </summary>
        private static void InitListMusic()
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(Directory.GetCurrentDirectory() + $@".\..\..\src\music");

                List<FileInfo> fileInfos = d.GetFiles("*.mp3").ToList();
                listMusic.Clear();

                Debug.WriteLine("Songs found : ");
                foreach (FileInfo file in fileInfos)
                {
                    listMusic.Add(file.Name);
                    Debug.WriteLine(file.Name);

                }

            }
            catch (ArgumentNullException) { Debug.WriteLine("Null music directory"); }
            catch (ArgumentException) { Debug.WriteLine("Can't find music directory"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine("Can't find music directory"); }
            catch (PathTooLongException) { Debug.WriteLine("Music path too long"); }
            catch (Exception e) { Debug.WriteLine(e.StackTrace); }
        }

        /// <summary>
        /// play a random music from the list if list is not empty
        /// can't loop on the same music if list contains more than 1 music
        /// </summary>
        public static void PlayMusic()
        {


            if (listMusic.Count == 0)
            {
                Debug.WriteLine("No song to play");
                return;
            }

            string fileMusic = listMusic[random.Next(0, listMusic.Count)];

            //avoid infinit loop if only one song in folder
            if (listMusic.Count > 1)
            {
                while (lastMusicPlayed == fileMusic)
                {
                    fileMusic = listMusic[random.Next(0, listMusic.Count)];
                }

            }

            lastMusicPlayed = fileMusic;

            try
            {
                mediaPlayerMusic.Open(new Uri(Directory.GetCurrentDirectory() + $@".\..\..\src\music\{fileMusic}"));
                mediaPlayerMusic.Play();
                CurrentMusicName = fileMusic;

                MusicChanged(null, new MusicChangedEventArgs(fileMusic));

                Debug.WriteLine($"Now playing : {fileMusic}");



            }
            catch (FileNotFoundException) { Debug.WriteLine($"Can't load {fileMusic}.mp3"); }
            catch (UriFormatException) { Debug.WriteLine($"Can't load {fileMusic}.mp3"); }
            catch (Exception e) { Debug.WriteLine(e.StackTrace); }



        }

        /// <summary>
        /// play music again when last track ended -> loop 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void MediaPlayerMusic_MediaEnded(object sender, EventArgs e) => PlayMusic();
    }
}
