﻿using System;

namespace Sound
{
    /// <summary>
    /// class containing the EventArgs of MusicChanged event 
    /// Standard .NET event patterns
    /// </summary>
    public class MusicChangedEventArgs : EventArgs
    {
        public string MusicPlaying { get; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="musicPlaying">music name to pass through</param>
        public MusicChangedEventArgs(string musicPlaying)
        {
            MusicPlaying = musicPlaying;
        }
    }
}
