﻿using Modele.Ships;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Converter
{
    /// <summary>
    /// class converter
    /// </summary>
    public class ShipTypeToImageConv : IValueConverter
    {
        /// <summary>
        /// return Bitmapimage associated with value (ShipType)
        /// </summary>
        /// <param name="value">source value (ShipType) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>Bitmapimage associated with ShipType</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ShipType st = (ShipType)value;
            ShipTypeImage.DicoShipTypeBmp.TryGetValue(st.ToString().ToUpper(), out BitmapImage btm);
            return btm;
        }
        /// <summary>
        /// NotImplemented
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>NotImplemented</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
