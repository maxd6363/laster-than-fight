﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace Converter
{
    /// <summary>
    /// class converter string / string
    /// </summary>
    public class CasseStringConv : IValueConverter
    {
        /// <summary>
        /// replace '_' with ' ' and uppercase first letter, lowercase the others
        /// </summary>
        /// <param name="value">source value (string) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>reture string '_' with ' ' and uppercase first letter, lowercase the others</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null)
            {
                string str = (string)value;
                str=str.ToLower();
                str = Regex.Replace(str, @"_", " ");


                if (!string.IsNullOrEmpty(str))
                {
                    char[] array = str.ToCharArray();
                    array[0]=char.ToUpper(array[0]);

                    return new string(array);
                }


            }
            return value;


        }
        /// <summary>
        /// return same string value
        /// </summary>
        /// <param name="value">source value (string) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //not used
            return value;
        }
    }
}
