﻿using Modele.Ships;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Converter
{
    /// <summary>
    /// class converter double / brush
    /// </summary>
    public class ColorConv : IValueConverter
    {
            public static List<string> WarningList = new List<string>()
            {
                "#C4E538",
                "#A3CB38",
                "#009432",
                "#12CBC4",
                "#0652DD",
                "#FFC312",
                "#F79F1F",
                "#F79F1F",
                "#EE5A24",
                "#EA2027"
            };


        /// <summary>
        /// hex to SolidColorBrush RGB
        /// </summary>
        /// <param name="hex">hex value like #444444</param>
        /// <returns>SolidColorBrush with RGB</returns>
        public static SolidColorBrush HexToColorRGB(string hex)
        {
            if (hex.Substring(0, 1) != "#")
            {
                throw new ArgumentException("hex value must start with #");
            }
            byte R = System.Convert.ToByte(hex.Substring(1, 2), 16);
            byte G = System.Convert.ToByte(hex.Substring(3, 2), 16);
            byte B = System.Convert.ToByte(hex.Substring(5, 2), 16);
            return new SolidColorBrush(Color.FromRgb(R, G, B));
        }
        /// <summary>
        /// hex to SolidColorBrush ARGB
        /// </summary>
        /// <param name="hex">hex value like #44444444</param>
        /// <returns>SolidColorBrush with ARGB</returns>
        public static SolidColorBrush HexToColorARGB(string hex)
        {
            if (hex.Substring(0, 1) != "#")
            {
                throw new ArgumentException("hex value must start with #");
            }
            byte A = System.Convert.ToByte(hex.Substring(1, 2), 16);
            byte R = System.Convert.ToByte(hex.Substring(3, 2), 16);
            byte G = System.Convert.ToByte(hex.Substring(5, 2), 16);
            byte B = System.Convert.ToByte(hex.Substring(7, 2), 16);
            return new SolidColorBrush(Color.FromArgb(A, R, G, B));
        }

        /// <summary>
        /// return warning color associate to value
        /// </summary>
        /// <param name="value">source value (double) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>SolidColorBrush associate to value (green - blue - yellow - red) </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double x = (double)value;
            double MaxSlider = (ShipFactory.TABHEIGHT * ShipFactory.TABWIDTH) / 2;
            int xClamp = (int)((x - 1) / (MaxSlider - 1) * (9 - 0) + 0);
            return HexToColorRGB(WarningList[xClamp]);
        }
        /// <summary>
        /// NotImplemented
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>NotImplemented</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
