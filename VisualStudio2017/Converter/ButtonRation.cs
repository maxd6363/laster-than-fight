﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Converter
{
    /// <summary>
    /// converter class double / double
    /// </summary>
    public class ButtonRation : IValueConverter
    {
        /// <summary>
        /// ratio calculation 7/3 double to double
        /// </summary>
        /// <param name="value">source value (double) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>value * 7/3</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value * 7 / 3;
        }
        /// <summary>
        /// ratio calculation 3/7 double to double
        /// </summary>
        /// <param name="value">source value (double) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>value * 3/7</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //never used
            return (double)value*3/7;
        }
    }
}
