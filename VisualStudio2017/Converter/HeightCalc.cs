﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Converter
{
    /// <summary>
    /// Class converter double / double
    /// </summary>
    public class HeightCalc : IValueConverter
    {
        /// <summary>
        /// Calculate value / 3 : double to double
        /// </summary>
        /// <param name="value">source value (double) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>value / 3</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value / 3;
        }
        /// <summary>
        /// Calculate value * 3
        /// </summary>
        /// <param name="value">source value (double) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>value * 3</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Never used
            return (double)value * 3;
        }
    }
}
