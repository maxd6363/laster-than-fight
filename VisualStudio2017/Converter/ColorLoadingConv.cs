﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Converter
{
    public class ColorLoadingConv : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double x = (double)value;
            int ix = (int)(x/30) % ColorConv.WarningList.Count;

            return ColorConv.HexToColorRGB(ColorConv.WarningList[ix]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //not used
            throw new NotImplementedException();
        }
    }
}
