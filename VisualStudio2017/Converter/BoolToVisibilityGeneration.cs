﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Converter
{
    /// <summary>
    /// Converts class between bool and visibility
    /// </summary>
    public class BoolToVisibilityGeneration : IValueConverter
    {
        /// <summary>
        /// Converts a boolean to a visibility for the Generator
        /// </summary>
        /// <param name="value">source value (bool) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns> true  : return "SAVE" - false : return "GENERATE"</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Hidden;
        }
        /// <summary>
        /// converter string to bool not used
        /// </summary>
        /// <param name="value">source value (string) </param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>return false</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //not used
            return false;
        }
    }
}
