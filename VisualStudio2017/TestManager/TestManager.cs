﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modele;
using Modele.Ressources;
using Modele.Ressources.Type.Metal;
using Modele.Ships;
using Persistent;

namespace TestManager
{
    [TestClass]
    public class TestManager
    {

        private ManagerModele managerModele = new ManagerModele(new LoadTxt(), new SaveTxt());


        /// <summary>
        /// Add ship and verify on .Count
        /// </summary>
        [TestMethod]
        public void TestAddShipCount()
        {
            Ship ship = ShipFactory.MakeAShip("Ship 1 ", 2, ShipType.ALL, 50);
            int lenght = ManagerModele.ListShip.Count;

            ManagerModele.CurrentShip = ship;
            ManagerModele.AddShip();
            Assert.AreEqual(lenght + 1, ManagerModele.ListShip.Count);
        }

        /// <summary>
        /// Add ship to the list and compare the last element of the list to this ship
        /// </summary>
        [TestMethod]
        public void TestAddShipLast()
        {
            Ship ship = ShipFactory.MakeAShip("Ship 1 ", 2, ShipType.ALL, 50);
            ManagerModele.CurrentShip = ship;


            ManagerModele.AddShip();
            Assert.AreEqual(ship,ManagerModele.ListShip.LastOrDefault());
        }


        /// <summary>
        /// Delete the ship and verify on .Count
        /// </summary>
        [TestMethod]
        public void TestRemoveShipCount()
        {
            Ship ship = ShipFactory.MakeAShip("Ship 1 ", 2, ShipType.ALL, 50);
            ManagerModele.CurrentShip = ship;

            ManagerModele.AddShip();

            int lenght = ManagerModele.ListShip.Count;

            ManagerModele.DeleteShip(ship);
            Assert.AreEqual(lenght - 1, ManagerModele.ListShip.Count);
        }

        /// <summary>
        /// Delete a "null" ship and verify on .Count
        /// </summary>
        
        [TestMethod]
        public void TestRemoveShipNull()
        {
            int lenght = ManagerModele.ListShip.Count;

            ManagerModele.DeleteShip(null);
            Assert.AreEqual(lenght, ManagerModele.ListShip.Count);
        }
        /// <summary>
        /// Test loading Ships wizthkxout a missing file and verify on count
        /// </summary>
        [TestMethod]
        public void TestLoadMissingFileSaveShip()
        {
            //fichier SaveShip.dat missing
            managerModele.Load();
            Assert.AreEqual(0, ManagerModele.ListShip.Count);
        }

        /// <summary>
        /// Test loading Ressource with a missing file, verify that ressources are succesfully loaded and init. to 0
        /// </summary>
        [TestMethod]
        public void TestLoadMissingFileSaveRessource()
        {
            //fichier SaveRessources.dat missing
            managerModele.Load();
            foreach(Ressource res in ManagerModele.ListRessource)
            {
                Assert.AreEqual(0, res.NbRessourcePlayer);
            }
        }

        /// <summary>
        /// Test addRessource regular
        /// </summary>
        [TestMethod]
        public void TestAddRessource()
        {
            RessourceMetal r = new RessourceMetal(RessourceMetalType.IRON);

            ManagerModele.AddRessource(new RessourceMetal(RessourceMetalType.IRON), 500);
            Assert.AreEqual(500, ManagerModele.ListRessource.Last(res => res.Equals(r)).NbRessourcePlayer);
        }


        /// <summary>
        /// Test addRessource negative
        /// </summary>
        [TestMethod]
        public void TestAddRessourceNegative()
        {
            RessourceMetal r = new RessourceMetal(RessourceMetalType.IRON);

            ManagerModele.AddRessource(new RessourceMetal(RessourceMetalType.IRON), -50000);
            Assert.AreEqual(0, ManagerModele.ListRessource.Last(res => res.Equals(r)).NbRessourcePlayer);
        }


    }
}
