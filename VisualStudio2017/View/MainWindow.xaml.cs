﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using View.Manager;
using View.PageView;
using Persistent;
using Modele;
using DynamicBackground;
using Sound;
using System.Text.RegularExpressions;
using View.Properties;
using System.Windows.Threading;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public bool isOnMainPage = false;
        private DoubleAnimation doubleAnimation;
        private Storyboard storyboard;

        private DoubleAnimation doubleAnimationFrame;
        private Storyboard storyboardFrame;

        public ManagerModele MyManager { get; }



        /// <summary>
        /// ctor for mainwindow
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            MyFrame.NavigationService.Navigate(new Loading());

            DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };
            timer.Start();
            timer.Tick += (sender, args) =>
            {
                timer.Stop();
                MyFrame.NavigationService.Navigate(new MainMenu());
            };

            ManagerResolution.Update();
            if (!Settings.Default.highContrast)
            {
                LoadBackground();
            }

            InitIcon();
            InitKey();
            InitPopupSong();
            InitAnimationBetweenFrame();

            MyManager = new ManagerModele(new LoadTxt(), new SaveTxt());
            MyManager.Load();

            SoundManager.MusicChanged += SoundManager_MusicChanged;
            SoundManager.PlayMusic();
        }


        /// <summary>
        /// methode called when MusicChanged event is send by SoundManager
        /// every '_' are replaced with ' ' and the ".mp3" is cut
        /// </summary>
        /// <param name="sender">Sender of the event, it will always be null because SoundManager is static</param>
        /// <param name="e">Args containing the name of the current music</param>
        private void SoundManager_MusicChanged(object sender, MusicChangedEventArgs e)
        {
            if (e != null)
            {
                PopupMusic.Text = Regex.Replace(e.MusicPlaying, "_", " ").Remove(e.MusicPlaying.Length - 4);
            }
            else
            {
                PopupMusic.Text = "Unknown Music";
            }
            InitAnimation();
        }

        /// <summary>
        /// load background Image
        /// </summary>
        internal void LoadBackground()
        {
            try
            {
                Background = new ImageBrush(new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\Background\MAIN_MENU.png")));
            }
            catch (ArgumentException) { Debug.WriteLine(@"Can't load src\img\Background\MAIN_MENU.png"); }
            catch (DirectoryNotFoundException) { Debug.WriteLine(@"Can't load src\img\Background\MAIN_MENU.png(folder)"); }
            catch (FileNotFoundException) { Debug.WriteLine(@"Can't find src\img\Background\MAIN_MENU.png(file)"); }

        }


        /// <summary>
        /// set background to white
        /// </summary>
        internal void UnloadBackground()
        {
            Background = new SolidColorBrush(Colors.White);
        }


        /// <summary>
        /// load icon on main menu
        /// </summary>
        private void InitIcon()
        {
            try
            {
                this.Icon = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\Icons\WINDOW_ICON.png"));
            }
            catch (DirectoryNotFoundException) { Debug.WriteLine(@"Can't find src\img\Icons\WINDOW_ICON.png (folder)"); }
            catch (FileNotFoundException) { Debug.WriteLine(@"Can't find src\img\Icons\WINDOW_ICON.png(file)"); }
            catch (Exception e) { Debug.WriteLine(e.StackTrace); }

        }




        /// <summary>
        /// register event of KeyEventHandler
        /// </summary>
        private void InitKey()
        {
            this.KeyDown += new KeyEventHandler(MainWindow_KeyDown);
        }


        /// <summary>
        /// set opaticity of popup to 
        /// </summary>
        private void InitPopupSong()
        {
            PopupMusic.Opacity = 0;
        }





        /// <summary>
        /// init the animation of popup
        /// </summary>
        private void InitAnimation()
        {
            doubleAnimation = new DoubleAnimation();
            storyboard = new Storyboard();

            doubleAnimation.From = 5;
            doubleAnimation.To = 0;
            doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(10));

            //storyBoard to generate animation
            storyboard.Children.Add(doubleAnimation);
            Storyboard.SetTargetName(doubleAnimation, "PopupMusic");
            Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(TextBlock.OpacityProperty));

            //begin the animation
            storyboard.Begin(this);
        }




        /// <summary>
        /// load the animation between 2 pages
        /// </summary>
        private void InitAnimationBetweenFrame()
        {
            doubleAnimationFrame = new DoubleAnimation();
            storyboardFrame = new Storyboard();
        }





        /// <summary>
        /// start animation between 2 pages
        /// </summary>
        public void AnimationBetweenFrame()
        {

            doubleAnimationFrame.From = 0.3;
            doubleAnimationFrame.To = 1;
            doubleAnimationFrame.Duration = new Duration(TimeSpan.FromMilliseconds(500));

            //storyBoard to generate animation
            storyboardFrame.Children.Add(doubleAnimationFrame);
            Storyboard.SetTargetName(doubleAnimationFrame, "MyFrame");
            Storyboard.SetTargetProperty(doubleAnimationFrame, new PropertyPath(Frame.OpacityProperty));

            //begin the animation
            storyboardFrame.Begin(this);
        }



        /// <summary>
        /// key down handler 
        /// B : if on main menu -> secret background reveled
        /// ESC : go to main menu
        /// F11 : change fullscreen state if not on settings page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.B)
            {
                if (MyFrame.Content.ToString() == typeof(MainMenu).ToString())
                {
                    MyFrame.NavigationService.Navigate(new Background());
                    UnloadBackground();
                }
                if (MyFrame.Content.ToString() == typeof(Background).ToString())
                {
                    MyFrame.NavigationService.Navigate(new MainMenu());
                    if (!Settings.Default.highContrast)
                    {
                        LoadBackground();
                    }
                }
            }

            if (e.Key == Key.Escape)
            {
                MyFrame.NavigationService.Navigate(new MainMenu());
            }

            if (e.Key == Key.A)
            {
                if (MyFrame.Content.ToString() != typeof(Play).ToString())
                {
                    MyFrame.NavigationService.Navigate(new Accessibility());
                }
            }

            if (e.Key == Key.O)
            {
                if (MyFrame.Content.ToString() != typeof(Play).ToString())
                {
                    MyFrame.NavigationService.Navigate(new Setting());
                }
            }

            if (e.Key == Key.F11)
            {
                //can't F11 in setting page because there is a button for this
                if (MyFrame.Content.ToString() != typeof(Setting).ToString())
                {
                    ManagerResolution.SetFullscreen(!ManagerResolution.IsFullscreen());
                    ManagerResolution.Update();
                    MyFrame.Refresh();

                }

            }
        }





    }
}
