﻿using System.Windows.Media;

namespace View.Manager
{
    /// <summary>
    /// static class containing some SolidColorBrush for theme
    /// </summary>
    public static class ManagerTheme
    {
        public static SolidColorBrush TextBoxBackground = new SolidColorBrush(Color.FromArgb(200, 52, 73, 94));
        public static SolidColorBrush TextBoxForeground = new SolidColorBrush(Color.FromArgb(255, 200, 200, 200));


    
    }
}
