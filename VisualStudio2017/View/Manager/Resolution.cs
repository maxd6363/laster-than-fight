﻿using System;
using System.Diagnostics;

namespace View.Manager
{
    /// <summary>
    /// resolution class
    /// </summary>
    public class Resolution : IEquatable<Resolution>
    {
        public int ResolutionHeight { get; }
        public int ResolutionWidth { get; }

        /// <summary>
        /// ctor for resolution
        /// </summary>
        /// <param name="width">width of screen</param>
        /// <param name="height">height of screen</param>
        public Resolution(int width, int height)
        {

            if (width < 640)
            {
                Debug.WriteLine($"Width Resolution too small : {width}  -> Set to 640");
                width = 640;
            }
            if (height < 320)
            {
                Debug.WriteLine($"Height Resolution too small : {height}  < 320  -> Set to 320");
                height = 320;
            }
            this.ResolutionWidth = width;
            this.ResolutionHeight = height;

        }
        /// <summary>
        /// default ctor, set to 1600x900
        /// </summary>
        public Resolution() : this(1600, 900)
        { }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;
            if (obj.GetType() == GetType()) return Equals(obj as Resolution);
            return false;
        }

        public bool Equals(Resolution other)
        {
            if (other.ResolutionHeight == ResolutionHeight &&
                other.ResolutionWidth == ResolutionWidth)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return ResolutionHeight.GetHashCode() + ResolutionWidth.GetHashCode();
        }

        public override string ToString()
        {
            return $"{ResolutionWidth} x {ResolutionHeight}";
        }

   
    }
}
