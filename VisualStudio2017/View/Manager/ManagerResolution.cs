﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace View.Manager
{

    /// <summary>
    /// static manager class handling the resolution of the app
    /// </summary>
    public static class ManagerResolution
    {
        public static List<Resolution> ListResolution { get; set; }


        /// <summary>
        /// static ctor, init list of resolution available
        /// </summary>
        static ManagerResolution()
        {
            InitResolutionList();
        }

        /// <summary>
        /// init list of resolution available
        /// </summary>
        private static void InitResolutionList()
        {
            ListResolution = new List<Resolution>
            {
                new Resolution(960, 540),
                new Resolution(1280, 720),
                new Resolution(1366, 768),
                new Resolution(1600, 900),
                new Resolution(1920, 1080),
                new Resolution(2560, 1440),
                new Resolution(3840, 2160)
            };
        }

      

        /// <summary>
        /// update resolution of mainWindow with Height and Width in settings
        /// </summary>
        private static void UpdateResolution()
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            if (mainWindow == null) { return; }
            mainWindow.Height = Properties.Settings.Default.resolutionHeight;
            mainWindow.Width = Properties.Settings.Default.resolutionWidth;
        }

        /// <summary>
        /// get current resolution in settings
        /// </summary>
        /// <returns>current resolution in settings</returns>
        public static Resolution GetCurrentResolution()
        {
            return new Resolution(Properties.Settings.Default.resolutionWidth, Properties.Settings.Default.resolutionHeight);
        }


        /// <summary>
        /// set resolution of mainWIndow
        /// </summary>
        /// <param name="r">resolution to set</param>
        public static void SetResolution(Resolution r)
        {
            if (r == null) { throw new ArgumentNullException(nameof(r)); }

            if (ListResolution.Contains(r))
            {
                Properties.Settings.Default.resolutionHeight = r.ResolutionHeight;
                Properties.Settings.Default.resolutionWidth= r.ResolutionWidth;
                Properties.Settings.Default.Save();
                
                UpdateResolution();
            }


        }

        /// <summary>
        /// set state of fullscreen
        /// </summary>
        /// <param name="setter">state</param>
        public static void SetFullscreen(bool setter)
        {
            Properties.Settings.Default.fullscreen = setter;
            Properties.Settings.Default.Save();
            Update();

        }

        /// <summary>
        /// check fullscreen state
        /// </summary>
        /// <returns>state : true -> app in fullscreen, false -> if not</returns>
        public static bool IsFullscreen()
        {
            return Properties.Settings.Default.fullscreen;
        }

        /// <summary>
        /// update mainWindow to fullscreen state in Settings
        /// </summary>
        private static void UpdateFullscreen()
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            if (mainWindow == null) { return; }

            if (IsFullscreen())
            {
                mainWindow.WindowStyle = WindowStyle.None;
                mainWindow.WindowState = WindowState.Maximized;
            }
            else
            {
                mainWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                mainWindow.WindowState = WindowState.Normal;
            }
        }

        /// <summary>
        /// update resolution and fullscreen state
        /// </summary>
        public static void Update()
        {
            UpdateFullscreen();
            UpdateResolution();
        }

    }
}
