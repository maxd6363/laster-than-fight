﻿using Modele.Rooms.Filler;
using Modele.Rooms.Sonic;
using Modele.Rooms.Tech;
using Modele.Rooms.Weapon;
using System.Windows;
using System.Windows.Controls;

namespace View
{
    internal class RoomTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FillerTemplate { get; set; }
        public DataTemplate NormalTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is RoomFiller) return FillerTemplate;
            if (item is RoomSonic || item is RoomTech || item is RoomWeapon) return NormalTemplate;
            return base.SelectTemplate(item, container);
        }
    }
}
