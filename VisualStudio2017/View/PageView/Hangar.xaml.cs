﻿using Modele;
using Modele.Ships;
using Sound;
using System.Windows;
using System.Windows.Controls;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Hangar.xaml
    /// </summary>
    
    public partial class Hangar : Page
    {
        /// <summary>
        /// ctor init Hangar page
        /// </summary>
        public Hangar()
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            InitializeComponent();
            DataContext = mainWindow.MyManager;
        }


        /// <summary>
        /// delete ship directly on listview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteShipButtonList(object sender, RoutedEventArgs e)
        {
            SoundManager.DeleteSound();
            if (sender is Button button)
            {
                if (button.DataContext is Ship s)
                {
                    ManagerModele.DeleteShip(s);
                }
            }
        }


        /// <summary>
        /// ship list viewer event "Selection changed" : click sound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShipListViewer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SoundManager.ClickSound();
            MyUserControl.Ship = ManagerModele.CurrentShip;
        }

        /// <summary>
        /// Hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteDatatemplateBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.DeleteHover();
        }
    }
}
