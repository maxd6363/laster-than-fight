﻿using Modele;
using Modele.Ships;
using Sound;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using View.Properties;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Generator.xaml
    /// </summary>
    public partial class Generator : Page, INotifyPropertyChanged
    {
        public int MaxSlider { get; }
        public int InitialValue { get; }
        private bool saving;
        public bool Saving
        {
            get
            {
                return saving;
            }
            set
            {
                if (value != saving)
                {
                    saving = value;
                    SavingOpposite = !value;
                    NotifyPropertyChanged();
                }
            }
        }

        //used for button switching
        public bool SavingOpposite
        {
            get
            {
                return !saving;
            }
            set
            {
                NotifyPropertyChanged();
            }
        }

        public int SliderHeight { get; }
        public int ThumbHeight { get; }


        private ShipType shipType;
        public ShipType ShipType
        {
            get
            {
                return shipType;
            }
            set
            {
                if (value != shipType)
                {
                    shipType = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private ManagerModele MyManager
        {
            get
            {
                return (Application.Current.MainWindow as MainWindow).MyManager;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly Random random = new Random();

        /// <summary>
        /// Ctor init generator page
        /// </summary>
        public Generator()
        {
            SliderHeight = 8;
            ThumbHeight = 30;
            MaxSlider = (ShipFactory.TABHEIGHT * ShipFactory.TABWIDTH) / 2;
            InitialValue = random.Next(10, MaxSlider);
            Saving = false;
            ShipType = ShipType.SONIC;

            InitializeComponent();
            DataContext = this;
            IsVisibleChanged += Generator_IsVisibleChanged;

        }



        /// <summary>
        /// Number of room event "Value changed"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NbRoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SoundManager.ClickSound();
        }


        /// <summary>
        /// percent of filler room event "Value changed"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PercentFillerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SoundManager.ClickSound();
        }

        /// <summary>
        /// generate button event "Click" : generate a new ship 
        /// if saving state == true -> add ship to collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GenerateButton(object sender, RoutedEventArgs e)
        {
            switch (ShipType)
            {
                case ShipType.ALL:
                    SoundManager.VoiceEverything();
                    break;
                case ShipType.SONIC:
                    SoundManager.VoiceSpeed();
                    break;
                case ShipType.WEAPON:
                    SoundManager.VoiceWeapon();
                    break;
                case ShipType.TECH:
                    SoundManager.VoiceTech();
                    break;
                default: SoundManager.EventSound();
                    break;
            }


            ManagerModele.MakeAShip(null, (int)NbRoomSlider.Value, ShipType, (int)PercentFillerSlider.Value);
            Saving = true;
            MyUserControl.Ship = ManagerModele.CurrentShip;
            MyUserControl.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// save button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();
            ManagerModele.AddShip();
            Saving = false;
            if (Settings.Default.autoSave)
            {
               MyManager.SaveShips();
               MyManager.SaveRessources();
            }
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.SaveHover();
        }

        /// <summary>
        /// discard button event "click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DiscardBtn_Click(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();
            MyUserControl.Visibility = Visibility.Hidden;
            Saving = false;
            ManagerModele.CurrentShip = null;

        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DiscardBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.DiscardHover();
        }




        /// <summary>
        /// Sonic type button of ship event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ShipType = ShipType.SONIC;
            SoundManager.ClickSound();
        }


        /// <summary>
        /// Tech type button of ship event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ShipType = ShipType.TECH;
            SoundManager.ClickSound();
        }


        /// <summary>
        /// Weapon type button of ship event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ShipType = ShipType.WEAPON;
            SoundManager.ClickSound();
        }

        /// <summary>
        /// All type button of ship event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ShipType = ShipType.ALL;
            SoundManager.ClickSound();
        }


        /// <summary>
        /// send event when property change
        /// </summary>
        /// <param name="propertyName">property changed</param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GenerateBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.GenerateHover();
        }




        /// <summary>
        /// speed hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.SpeedHover();
        }

        /// <summary>
        /// tech hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.TechHover();
        }

        /// <summary>
        /// weapon hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_2(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.WeaponHover();
        }

        /// <summary>
        /// everything hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_3(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.EverythingHover();
        }




        /// <summary>
        /// visibility changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Generator_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

            //page not focus
            if (Saving)
            {
                //ship not saved
                ManagerModele.CurrentShip = null;
            }

        }

    }
}



