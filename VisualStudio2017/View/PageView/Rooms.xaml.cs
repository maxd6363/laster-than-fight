﻿using Sound;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Rooms.xaml
    /// </summary>
    public partial class Rooms : Page
    {
        private DoubleAnimation doubleAnimation;
        private Storyboard storyboard;


        /// <summary>
        /// ctor of rooms page
        /// </summary>
        public Rooms()
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;

            InitializeComponent();
            DataContext = mainWindow.MyManager;
            GridView.Visibility = Visibility.Hidden;
            InitAnimation();
        }




       /// <summary>
       /// init animation for room view
       /// </summary>
        private void InitAnimation()
        {
            doubleAnimation = new DoubleAnimation();
            storyboard = new Storyboard();

            //delegate to Get ActualWidth
            Loaded += delegate
            {
                double from = GridView.ActualWidth / 3;
                double to = GridView.ActualWidth / 2.5;

                //doubleAnimation for bouncing image
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.AutoReverse = true;

                //Animation depending of the current size
                doubleAnimation.From = from;
                doubleAnimation.To = to;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));

                //storyBoard to generate animation
                storyboard.Children.Add(doubleAnimation);
                Storyboard.SetTargetName(doubleAnimation, "RoomsImage");
                Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(Image.WidthProperty));

                //begin the animation
                storyboard.Begin(this);

            };

        }
        /// <summary>
        /// room list viewer event "selection changed" -> click sound + Selected room grid visible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomsListViewer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SoundManager.ClickSound();
            GridView.Visibility = Visibility.Visible;
        }
    }
}
