﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Loading.xaml
    /// </summary>
    public partial class Loading : Page
    {

        private DoubleAnimation doubleAnimation;
        private Storyboard storyboard;

        public Loading()
        {
            InitializeComponent();
            InitAnimation();

        }

        private void InitAnimation()
        {
            Loaded += delegate
             {
                 doubleAnimation = new DoubleAnimation();
                 storyboard = new Storyboard();

                 doubleAnimation.From = 0;
                 doubleAnimation.To = GridLoading.ActualWidth *0.95;
                 doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1.5));

                //storyBoard to generate animation
                storyboard.Children.Add(doubleAnimation);
                 Storyboard.SetTargetName(doubleAnimation, "FalseLoading");
                 Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(Rectangle.WidthProperty));

                //begin the animation
                storyboard.Begin(this);

             };
        }

    }
}
