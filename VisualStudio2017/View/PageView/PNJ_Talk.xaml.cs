﻿using Modele;
using Sound;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for PNJ_Talk.xaml
    /// </summary>
    public partial class PNJ_Talk : Page
    {

        public PNJText MyPNJText { get; } = new PNJText();
        private BitmapImage imageNico;

        public BitmapImage Overlay { get; private set; }
        private string Path { get; } = Directory.GetCurrentDirectory() + @".\..\..\src\img\PNJ\";
        private readonly Storyboard pathAnimationStoryboard = new Storyboard();

        public PNJ_Talk()
        {
            LoadImage();
            InitializeComponent();
            DataContext = this;
            InitAnimation();
            pathAnimationStoryboard.Completed += PathAnimationStoryboard_Completed;
                   
        }

        /// <summary>
        /// load image
        /// </summary>
        void LoadImage()
        {
            try
            {
                imageNico = new BitmapImage(new Uri(Path + "NICOR.png"));
                Overlay = new BitmapImage(new Uri(Path + "OVERLAY.png"));
            }
            catch (Exception e) when (e is DirectoryNotFoundException || e is FileNotFoundException)
            {
                //Image not found
                try
                {
                    imageNico = new BitmapImage(new Uri(Path + "../NAN.png"));
                    Overlay = new BitmapImage(new Uri(Path + "../NAN.png"));
                }
                catch (DirectoryNotFoundException) { Debug.WriteLine("Can't load NAN.png (folder)"); }
                catch (FileNotFoundException) { Debug.WriteLine("Can't load NAN.png (file)"); }
            }

        }




        /// <summary>
        /// init the animation of nico
        /// </summary>
        void InitAnimation()
        {

            Loaded += delegate
            {
                // Create a NameScope for the page so that
                // we can use Storyboards.
                NameScope.SetNameScope(this, new NameScope());
                // Create a button.
                Image Navi = new Image
                {
                    Source = imageNico
                };

                // Create a MatrixTransform. This transform
                // will be used to move the button.
                MatrixTransform buttonMatrixTransform = new MatrixTransform();
                Navi.RenderTransform = buttonMatrixTransform;

                // Register the transform's name with the page
                // so that it can be targeted by a Storyboard.
                RegisterName("ButtonMatrixTransform", buttonMatrixTransform);
                mainPanel.Children.Add(Navi);

                // Create the animation path.
                PathGeometry animationPath = new PathGeometry();
                PathFigure pFigure = new PathFigure
                {
                    StartPoint = new Point(ActualWidth * 1.3, -ActualHeight)

                };
                LineSegment lineSegment = new LineSegment
                {
                    Point = new Point(ActualWidth * -0.6, ActualHeight)
                };

                PathSegmentCollection myPathSegmentCollection = new PathSegmentCollection
                {
                    lineSegment
                };

                pFigure.Segments = myPathSegmentCollection;

                PathFigureCollection myPathFigureCollection = new PathFigureCollection
                {
                    pFigure
                };


                animationPath.Figures = myPathFigureCollection;


                // Freeze the PathGeometry for performance benefits.
                animationPath.Freeze();

                // Create a MatrixAnimationUsingPath to move the
                // button along the path by animating
                // its MatrixTransform.
                MatrixAnimationUsingPath matrixAnimation =
                    new MatrixAnimationUsingPath
                    {
                        PathGeometry = animationPath,
                        Duration = TimeSpan.FromSeconds(3),
                    };

                Storyboard.SetTargetName(matrixAnimation, "ButtonMatrixTransform");
                Storyboard.SetTargetProperty(matrixAnimation, new PropertyPath(MatrixTransform.MatrixProperty));
                // Create a Storyboard to contain and apply the animation.
                pathAnimationStoryboard.Children.Add(matrixAnimation);


                // Start the storyboard
                pathAnimationStoryboard.Begin(this);

            };


        }


        /// <summary>
        /// animation loop one time
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PathAnimationStoryboard_Completed(object sender, EventArgs e)
        {
            if (IsVisible)
            {
                SoundManager.PassNicomete();
                pathAnimationStoryboard.Begin(this);
            }
        }






    }
}
