﻿using DynamicBackground;
using Sound;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using View.Manager;
using View.Properties;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Setting.xaml
    /// </summary>
    public partial class Setting : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public int SliderHeight { get; }
        public int ThumbHeight { get; }

        private string autoSaveText;
        public string AutoSaveText
        {
            get { return autoSaveText; }
            set
            {
                if (value != autoSaveText)
                {
                    autoSaveText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string dynamicBackgroundText;
        public string DynamicBackgroundText
        {
            get { return dynamicBackgroundText; }
            set
            {
                if (value != dynamicBackgroundText)
                {
                    dynamicBackgroundText = value;
                    NotifyPropertyChanged();
                }
            }
        }





        /// <summary>
        /// ctor for settings page
        /// </summary>
        public Setting()
        {
            SliderHeight = 8;
            ThumbHeight = 30;
            AutoSaveText = Settings.Default.autoSave ? "Auto Save : ON" : "Auto Save : OFF";
            DynamicBackgroundText = Settings.Default.dynamicBackground ? "Dynamic\nBackground : ON" : "Dynamic\nBackground : OFF";


            InitializeComponent();
            DataContext = this;
            ResolutionComboBox.ItemsSource = ManagerResolution.ListResolution;

            UpdateResolutionCombobox();
            UpdateSliders();

            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.AnimationBetweenFrame();
        }


        /// <summary>
        /// update is resolution combo box is enable or not based on fullscreen state -> can't change resolution in fullscreen
        /// </summary>
        private void UpdateResolutionCombobox()
        {
            ResolutionComboBox.Text = ManagerResolution.GetCurrentResolution().ToString();
            if (ManagerResolution.IsFullscreen())
            {
                ResolutionComboBox.IsEnabled = false;
            }
            else
            {
                ResolutionComboBox.IsEnabled = true;
            }
        }



        /// <summary>
        /// update value of Music and Sound slider with value in Settings in Sound assembly
        /// </summary>
        private void UpdateSliders()
        {
            MusicVolumeSlider.Value = SoundManager.MusicVolume;
            SoundVolumeSlider.Value = SoundManager.SoundVolume;
        }


        /// <summary>
        /// return button event "Click" -> go to Main Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReturnButton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());

            SoundManager.BackSound();

        }

        /// <summary>
        /// fullscreen button event "Click" -> change fullscreen state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FullscreenButton(object sender, RoutedEventArgs e)
        {
            ManagerResolution.SetFullscreen(!ManagerResolution.IsFullscreen());
            UpdateResolutionCombobox();
            SoundManager.EventSound();
        }

        /// <summary>
        /// resolution combo box event "Selection changed" -> update resolution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResolutionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ManagerResolution.SetResolution(ResolutionComboBox.SelectedItem as Resolution);
            UpdateResolutionCombobox();
            SoundManager.EventSound();



        }
        /// <summary>
        /// load button event "Click" -> load from file SaveShip.dat & SaveRessource.dat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton(object sender, RoutedEventArgs e)
        {

            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.MyManager.Load();

            SoundManager.EventSound();


        }

        /// <summary>
        /// dynamic background event "Click" -> change state of dynamic background on main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DynamicBackgroundButton(object sender, RoutedEventArgs e)
        {
            Settings.Default.dynamicBackground = !Settings.Default.dynamicBackground;
            Settings.Default.Save();
            BackgroundManager.ChangeState();
            DynamicBackgroundText = Settings.Default.dynamicBackground ? "Dynamic\nBackground : ON" : "Dynamic\nBackground : OFF";

        }


        /// <summary>
        /// auto save button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            Settings.Default.autoSave = !Settings.Default.autoSave;
            Settings.Default.Save();
            AutoSaveText = Settings.Default.autoSave ? "Auto Save : ON" : "Auto Save : OFF";

        }



        /// <summary>
        /// music volume slider event "Value changed" -> update volume of music
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MusicVolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SoundManager.SetMusicVolume(MusicVolumeSlider.Value);
            SoundManager.CheckSound();
        }
        /// <summary>
        /// sound volume slider event "Value changed" -> update volume of sound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SoundVolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SoundManager.SetSoundVolume(SoundVolumeSlider.Value);
            SoundManager.CheckSound();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FullscreenBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.FullscreenHover();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DynamicBackgroundBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.BackgroundHover();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.LoadHover();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.BackHover();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoSaveBtn_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.AutoSaveHover();
        }




        /// <summary>
        /// send PropertyCanged event
        /// </summary>
        /// <param name="propertyName">name of the property</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        
    }
}
