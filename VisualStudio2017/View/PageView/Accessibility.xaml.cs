﻿using Sound;
using System.Windows;
using System.Windows.Controls;
using View.Properties;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Accessibility.xaml
    /// </summary>
    public partial class Accessibility : Page
    {
        /// <summary>
        /// ctor init page
        /// </summary>
        public Accessibility()
        {
            InitializeComponent();
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.AnimationBetweenFrame();
        }

        /// <summary>
        /// High Contrast button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighContrastButton(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();
            MainWindow main = (MainWindow)Application.Current.MainWindow;



            if (Settings.Default.highContrast)
            {
                main.LoadBackground();
            }
            else
            {
                main.UnloadBackground();
            }

            Settings.Default.highContrast = !Settings.Default.highContrast;
            Settings.Default.Save();

        }





        /// <summary>
        /// Narator button event "Click"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NarratorButton(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();
            SoundManager.SetNarator(!SoundManager.IsNaratorActive());
        }




        /// <summary>
        /// Return button event "Click" : go to main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReturnButton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());
            SoundManager.BackSound();
        }


        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.ContrastHover();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.NarratorHover();
        }
        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_2(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.BackHover();
        }
    }
}
