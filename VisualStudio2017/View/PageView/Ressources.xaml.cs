﻿using Modele;
using Modele.Ressources;
using Sound;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using View.Properties;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Ressources.xaml
    /// </summary>
    public partial class Ressources : Page, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private DoubleAnimation doubleAnimation;
        private Storyboard storyboard;

        public ManagerModele MyManager
        {
            get
            {
                return (Application.Current.MainWindow as MainWindow).MyManager;
            }
        }

      


        private int valueToAdd;
        public int ValueToAdd
        {
            get => valueToAdd;

            set
            {
                if (value != valueToAdd)
                {
                    valueToAdd = value;
                    NotifyPropertyChanged();
                }
            }
        }



        /// <summary>
        /// send PropertyCanged event
        /// </summary>
        /// <param name="propertyName">name of the property</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        /// <summary>
        /// ctr init Ressources page
        /// </summary>
        public Ressources()
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            InitializeComponent();
            DataContext = this;
            RessourcesListViewer.DataContext = mainWindow.MyManager;
            myCustomControl.AutoSave= Settings.Default.autoSave;

            InitAnimation();

            SelectedRessource.Visibility = Visibility.Hidden;

        }





        /// <summary>
        /// init animation of ressources
        /// </summary>
        private void InitAnimation()
        {
            doubleAnimation = new DoubleAnimation();
            storyboard = new Storyboard();


            //delegate to Get ActualWidth
            Loaded += delegate
            {

                double from = SelectedRessource.ActualWidth / 3;
                double to = SelectedRessource.ActualWidth / 2;

                //doubleAnimation for bouncing image
                doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
                doubleAnimation.AutoReverse = true;

                //Animation depending of the current size
                doubleAnimation.From = from;
                doubleAnimation.To = to;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));

                //storyBoard to generate animation
                storyboard.Children.Add(doubleAnimation);
                Storyboard.SetTargetName(doubleAnimation, "RessourcesImage");
                Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(Image.WidthProperty));

                //begin the animation
                storyboard.Begin(this);
            };
        }






        /// <summary>
        /// remove ValueToAdd ressources directly on list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinusRessourceList(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();

            if (sender is Button button)
            {
                if (button.DataContext is Ressource r)
                {
                    ManagerModele.AddRessource(r, -ValueToAdd);
                    if (Settings.Default.autoSave)
                    {
                        MyManager.SaveRessources();
                    }
                }
            }

        }

        /// <summary>
        /// Add ValueToAdd ressources directly on list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlusRessourceList(object sender, RoutedEventArgs e)
        {
            SoundManager.EventSound();


            if (sender is Button button)
            {
                if (button.DataContext is Ressource r)
                {
                    ManagerModele.AddRessource(r, ValueToAdd);
                    if (Settings.Default.autoSave)
                    {
                        MyManager.SaveRessources();
                    }
                }
            }
        }

        /// <summary>
        /// Ressource list viewer event "selection changed" -> click sound + set Selected Ressource grid to visible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RessourcesListViewer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedRessource.Visibility = Visibility.Visible;
            SoundManager.ClickSound();


        }



        /// <summary>
        /// Radio box checked event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Rb_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is RadioButton rb)
            {
                try
                {
                    ValueToAdd = int.Parse(rb.Content.ToString());

                }
                catch (ArgumentException) { ValueToAdd = 10; }
                catch (FormatException) { ValueToAdd = 10; }
                catch (OverflowException) { ValueToAdd = 10; }
            }
        }

        /// <summary>
        /// Hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.MinusHover();
        }

        /// <summary>
        /// Hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.PlusHover();
        }
    }
}
