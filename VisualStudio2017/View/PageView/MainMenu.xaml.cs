﻿using DynamicBackground;
using Sound;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        private readonly Random random = new Random();
        public MainMenu()
        {
            SoundManager.StartApp();
            InitializeComponent();
            InitBackground();
            LoadButtons();

            if (random.Next(0, 100) == 1) { LTF.Text = "HEY BRO !"; }
       

            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.AnimationBetweenFrame();
        }


        private void InitBackground()
        {
            if (BackgroundManager.Active)
            {
                BackgroundFrame.NavigationService.Navigate(new Background());
            }
            else
            {
                BackgroundFrame.NavigationService.Navigate(null);
            }

        }

        //Load button background
        private void LoadButtons()
        {
            //PLAY BUTTON
            try
            {
                PlayBtn.Background = new ImageBrush(new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\Icons\PLAY_BUTTON.png")));
            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine("File Not Found");
                PlayBtn.Background = new SolidColorBrush(Colors.Purple);
                PlayBtn.Content = "PLAY";
            }
            catch (DirectoryNotFoundException)
            {
                Debug.WriteLine("Folder Not Found");
                PlayBtn.Background = new SolidColorBrush(Colors.Purple);
                PlayBtn.Content = "PLAY";
            }

            //EXIT BUTTON
            try
            {
                ExitBtn.Background = new ImageBrush(new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\Icons\EXIT_BUTTON.png")));
            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine("File Not Found");
                ExitBtn.Background = new SolidColorBrush(Colors.Purple);
                ExitBtn.Content = "EXIT";
            }
            catch (DirectoryNotFoundException)
            {
                Debug.WriteLine("Folder Not Found");
                ExitBtn.Background = new SolidColorBrush(Colors.Purple);
                ExitBtn.Content = "EXIT";
            }

            //ACCESSIBILITY BUTTON
            try
            {
                AccessibilityBtn.Background = new ImageBrush(new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\Icons\ACCESSIBILITY_BUTTON.png")));
            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine("File Not Found");
                AccessibilityBtn.Background = new SolidColorBrush(Colors.Purple);
                AccessibilityBtn.Content = "ACCESSIBILITY";
            }
            catch (DirectoryNotFoundException)
            {
                Debug.WriteLine("Folder Not Found");
                AccessibilityBtn.Background = new SolidColorBrush(Colors.Purple);
                AccessibilityBtn.Content = "ACCESSIBILITY";
            }

            //SETTING BUTTON
            try
            {
                SettingBtn.Background = new ImageBrush(new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @".\..\..\src\img\Icons\SETTING_BUTTON.png")));
            }
            catch (FileNotFoundException)
            {
                Debug.WriteLine("File Not Found");
                SettingBtn.Background = new SolidColorBrush(Colors.Purple);
                SettingBtn.Content = "SETTING";
            }
            catch (DirectoryNotFoundException)
            {
                Debug.WriteLine("Folder Not Found");
                SettingBtn.Background = new SolidColorBrush(Colors.Purple);
                SettingBtn.Content = "SETTING";
            }
        }

        private void ExitButton(object sender, RoutedEventArgs e)
        {

            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.MyManager.Save();
            Environment.Exit(0);
        }
        private void PlayButton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Play());
            SoundManager.GoSound();
        }
        private void AccessibilityButton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Accessibility());
            SoundManager.GoSound();
        }
        private void SettingButton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Setting());
            SoundManager.GoSound();
        }

        private void PlayBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            SoundManager.PlayHover();
        }

        private void ExitBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            SoundManager.ExitHover();
        }

        private void AccessibilityBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            SoundManager.AccessibilityHover();
        }

        private void SettingBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            SoundManager.SettingsHover();
        }
    }
}
