﻿using Sound;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace View.PageView
{
    /// <summary>
    /// Interaction logic for Play.xaml
    /// </summary>
    public partial class Play : Page
    {

        /// <summary>
        /// ctor init Play page
        /// </summary>
        public Play()
        {
            InitializeComponent();
            DataContext = this;

            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            mainWindow.AnimationBetweenFrame();


            ContentFrame.NavigationService.Navigate(new PNJ_Talk());

        }




        /// <summary>
        /// Generator button event "Click" -> go to Generator page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GeneratorButton(object sender, RoutedEventArgs e)
        {
            ContentFrame.NavigationService.Navigate(new Generator());
            Generator.Background = new SolidColorBrush(Color.FromArgb(255,100,100,100));
            SoundManager.GoSound();
        }

        /// <summary>
        /// Hangar button event "Click" -> go to Hangar page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HangarButton(object sender, RoutedEventArgs e)
        {
            ContentFrame.NavigationService.Navigate(new Hangar());
            SoundManager.GoSound();

        }

        /// <summary>
        /// Rooms button event "Click" -> go to Rooms page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomsButton(object sender, RoutedEventArgs e)
        {
            ContentFrame.NavigationService.Navigate(new Rooms());
            SoundManager.GoSound();

        }

        /// <summary>
        /// Ressources button event "Click" -> go to Ressources page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RessourcesButton(object sender, RoutedEventArgs e)
        {
            ContentFrame.NavigationService.Navigate(new Ressources());
            SoundManager.GoSound();

        }

        /// <summary>
        /// Return button event "Click" -> go to Main menu page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReturnButton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());

        }


        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.GeneratorHover();
        }
        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.HangarHover();
        }
        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_2(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.RoomHover();
        }
        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_3(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.ResourceHover();
        }

        /// <summary>
        /// hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseEnter_4(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundManager.BackHover();
        }
    }
}
