﻿using Modele;
using Modele.Ships;
using Persistent;
using System;

namespace ShipGenerationTest
{
    class Program
    {
        private static ManagerModele Manager { get; set; }
        static void Main(string[] args)
        {
            Manager = new ManagerModele(new LoadTxt(), new SaveTxt());

            Console.WriteLine("Generating Ship : ");
            Console.WriteLine("(Loading can be long due to the missing src folder)");

            DispShip(ShipFactory.MakeAShip("MyShip1", 5000, ShipType.ALL, 50));
            DispShip(ShipFactory.MakeAShip("MyShip2", -2, ShipType.ALL, 50));
            DispShip(ShipFactory.MakeAShip("MyShip3", 50, ShipType.ALL, -50));
            DispShip(ShipFactory.MakeAShip("MyShip4", 50, ShipType.ALL, -50));

            Console.ReadLine();

        }



        /// <summary>
        /// display the ship to Debug output
        /// </summary>
        public static void DispShip(Ship ship)
        {
            try
            {
                Console.WriteLine($"Name : {ship.Name}");
                Console.WriteLine($"Number of rooms : {ship.NumberOfRoom}");
                Console.WriteLine($"Type : {ship.Type}");
                Console.WriteLine();


                for (int i = 0; i < ShipFactory.TABHEIGHT; i++)
                {
                    for (int j = 0; j < ShipFactory.TABWIDTH; j++)
                    {
                        if(ship.CompositionTab[i, j].Type != 0)
                        {
                            Console.Write(((int)ship.CompositionTab[i, j].Type).ToString("X") + " ");
                        }
                        else
                        {
                            Console.Write("  ");
                        }
                    }
                    Console.WriteLine();
                }
                Console.Write("\n\n\n");
            }
            catch (Exception) { }
        }

    }
}
