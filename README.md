~~~ README.md ~~~~


Laster Than Fight © ®


POULAIN Maxime G4 A1 IUT
ROULLAUD François G4 A1 IUT
 


/!\ WebException peut être lancée lors du chargement des images sur les PCs ayant le répertoire sur un disque réseau distant /!\
/!\ Installer l'application sur le disque C:/ doit résoudre ce bug /!\

---------------------------------- Documentation ----------------------------------

	Documentation Principale :
		./Documentation.pdf
		./Documentation/Contenu/Documentation.odt


	Vidéo :
		https://youtu.be/HmtpUrzbaGg



	Diagramme de classe :
		./Documentation/ClassDiagram/ClassDiagram.mdj
		./Documentation/ClassDiagram/ClassDiagram.png
		./Documentation/ClassDiagram/ClassDiagram.svg
		
		
	Diagramme de package :
		./Documentation/PackageDiagram/PackageDiagram.mdj
		./Documentation/PackageDiagram/PackageDiagram.png
		./Documentation/PackageDiagram/PackageDiagram.svg
		
		
	Diagramme de séquence :
		./Documentation/SequenceDiagram/SequenceDiagram.mdj
		./Documentation/SequenceDiagram/SequenceDiagram.png
		./Documentation/SequenceDiagram/SequenceDiagram.svg
		
		
	Diagramme de structure :
		./Documentation/StructureDiagram/StructureDiagram.mdj
		./Documentation/StructureDiagram/StructureDiagram.png
		./Documentation/StructureDiagram/StructureDiagram.svg
		
		
	Diagramme de cas d'utilisation :
		./Documentation/UseCaseDiagram/UseCaseDiagram.mdj
		./Documentation/UseCaseDiagram/UseCaseDiagram.png
		./Documentation/UseCaseDiagram/UseCaseDiagram.svg


	Sketch :
		./Documentation/Sketch/Sketch.bmpr
	
	Storyboard :
		./Documentation/Sketch/Storyboard.png


	Persona : 
		./Documentation/Persona/PERSONA_KevinAsfuk.pdf
		./Documentation/Persona/PERSONA_LaetytiaQuiquou.pdf
		./Documentation/Persona/PERSONA_LuisPadrass.pdf




---------------------------------- Visual Studio ----------------------------------
	
	Compiler et executer le projet "View" pour démarrer Laster Than Fight
	Compiler et executer le projet "ShipGenerationShip" pour démarrer le test console de génération de vaisseau (peut être long à démarrer sur certaine machine)
	Compiler et executer le projet "DynamicBackground" pour démarrer le fond d'écran dynamique et expérimenter avec les FPS, l'accrochage, le nombre de particules et l'épaisseur









---------------------------------- Setup ----------------------------------

	Setup : 
		./VisualStudio2017/Installer/Debug/setup.exe
		(debug inclu dans le dépos)
		
		./VisualStudio2017/Installer/Release/setup.exe
		(release non inclu dans le dépos, à compiler à la main)


---------------------------------- Remerciements ----------------------------------
	
	Un grand merci à RAYMOND Nicolas qui à su répondre à toutes nos questions ainsi que nous aider dans l'avancé de ce projet.
	
	